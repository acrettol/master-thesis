2023 events - Description of the Video camera screenshots

In Section 6.4.1
	- Figure 38a, CD27_07_12_Surges
	- Figure 38b, CD29_07_12_Surges

In Section 6.4.2
	- Figure 39a, CD27_06_10_Surge1
	- Figure 39b, CD29_06_10_Surge1

In Section 6.4.3
	- Figure 40,  11_14_CD27_front

In Section 6.5
	- Figure 42a, CD27_06_02_A_Start
	- Figure 42b, CD29_06_02_A_Start

In Section 7.4.2
	- Figure 46a, CD27_06_10_Surge2
	- Figure 46b, CD29_06_10_Surge2
	- Figure 47a, 06_10_CD27_PreSurging
	- Figure 47b, 06_10_CD29_PreSurging

In Section 7.4.3
	- Figure 49a, 11_14_CD27_pressurges
	- Figure 49b, 11_14_CD27_surges

In Section 7.4.6
	- Figure 51a, Front_Gazo_06_02_Cam3
	- Figure 51b, Front_CD27_07_12