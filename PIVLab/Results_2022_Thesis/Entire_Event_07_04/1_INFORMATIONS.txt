Entire event 04.07.2022 - Description of the Figures

In Section 5.2
	- Figure 28a, MeanArea_Gazo_07_04_22_Mod4
	- Figure 28b, MeanArea_Gazo_07_04_22_Mod3
	- Figure 28c, MeanArea_Gazo_07_04_22_Mod2
	- Figure 29,  Area_Square_07_04_22

In Appendix 6
	- Figure 61, MeanArea_Gazo_07_04_22_Preproc
	- Figure 61, MeanArea_Gazo_07_04_22_Mod6_Multi_AltiAzi

Unused
	- MeanArea_Gazo_07_04_22_Mod1
	- MeanArea_Gazo_07_04_22_Mod5_MultiAlti