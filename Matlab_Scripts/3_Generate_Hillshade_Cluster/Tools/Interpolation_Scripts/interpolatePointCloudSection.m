function [F] = interpolatePointCloudSection(ptCloud)
%%A function to create a scattered interpolant of an input point cloud
xyz = double(ptCloud.Location);
x = xyz(:,1);
y=xyz(:,2);
z=xyz(:,3);
F = scatteredInterpolant(x,y,z);
F.Method = 'linear';
F.ExtrapolationMethod = 'none';
end