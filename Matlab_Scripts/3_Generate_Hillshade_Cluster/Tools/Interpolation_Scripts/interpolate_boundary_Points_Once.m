function [interpolatedPtCloud,F] = interpolate_boundary_Points_Once(ptCloud,Xq,Yq,in_poly)
%%A function to create a scattered interpolant of an input point cloud, and
%%generate interpolated values based on input points and NaN mask
xyz = double(ptCloud.Location);
x = xyz(:,1);
y=xyz(:,2);
z=xyz(:,3);
tic
F = scatteredInterpolant(x,y,z);
F.Method = 'linear';
F.ExtrapolationMethod = 'none';


Zq = F(Xq,Yq);
z_1D = Zq(:);
tic
z_1D(~in_poly) = NaN;

Zq = reshape(z_1D,size(Xq));

ptCloudData = cat(3,Xq,Yq,Zq);
% ptCloudData = ptCloudData(ptCloudData(:,3)<zCutoff,:);
% ptCloudData = ptCloudData(ptCloudData(:,2)<0,:);

interpolatedPtCloud = pointCloud(ptCloudData);
% cmatrix = ones(size(interpolatedPtCloud.Location)).*[1 1 1];
% interpolatedPtCloud = pointCloud(ptCloudData,'Color',cmatrix);

end