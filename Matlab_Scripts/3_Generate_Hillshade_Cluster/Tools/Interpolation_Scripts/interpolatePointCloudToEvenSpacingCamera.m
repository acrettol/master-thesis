function [interpolatedPtCloud,F] = interpolatePointCloudToEvenSpacingCamera_Image(ptCloud,spacing,zCutoff)
%%A function to create a scattered interpolant of an input point cloud, and
%%generate interpolated values based on an input spacing
xyz = double(ptCloud.Location);
x = xyz(:,1);
y=xyz(:,2);
z=xyz(:,3);
F = scatteredInterpolant(x,y,z);
F.Method = 'linear';
F.ExtrapolationMethod = 'none';

[Xq,yQ] = meshgrid(min(x):spacing:max(x),min(y):spacing:0);
Zq = F(Xq,yQ);

ptCloudData = cat();
ptCloudData = ptCloudData(ptCloudData(:,3)<zCutoff,:);
ptCloudData = ptCloudData(ptCloudData(:,2)<0,:);

interpolatedPtCloud = pointCloud(ptCloudData);
cmatrix = ones(size(interpolatedPtCloud.Location)).*[1 1 1];
interpolatedPtCloud = pointCloud(ptCloudData,'Color',cmatrix);

end