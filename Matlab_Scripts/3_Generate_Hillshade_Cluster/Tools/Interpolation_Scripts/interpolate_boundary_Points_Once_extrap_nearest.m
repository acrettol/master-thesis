function [interpolatedPtCloud,pt_cloud_interp] = interpolate_boundary_Points_Once_extrap_nearest(ptCloud,Xq,Yq,in_poly,base_topography_interp)
%%A function to create a scattered interpolant of an input point cloud, and
%%generate interpolated values based on input points and NaN mask

xyz = double(ptCloud.Location);
x = xyz(:,1);
y=xyz(:,2);
z=xyz(:,3);

pt_cloud_interp = scatteredInterpolant(x,y,z);

Zq = zeros(size(Xq));
%%linear interpolate inside
pt_cloud_interp.Method = 'linear';
pt_cloud_interp.ExtrapolationMethod = 'nearest';
Zq(in_poly) = pt_cloud_interp(Xq(in_poly),Yq(in_poly));

%%nearest neighbor outside
extrap_points = ~in_poly;
pt_cloud_interp.Method = 'nearest';
pt_cloud_interp.ExtrapolationMethod = 'nearest';
Zq(extrap_points) = pt_cloud_interp(Xq(extrap_points),Yq(extrap_points));

%%keep base topography where
base_topo_points = base_topography_interp.Location(:,:,3) > Zq;
base_topo_z = base_topography_interp.Location(:,:,3);
Zq(base_topo_points) = base_topo_z(base_topo_points);
interpolatedPtCloud = pointCloud(cat(3,Xq,Yq,Zq));

end