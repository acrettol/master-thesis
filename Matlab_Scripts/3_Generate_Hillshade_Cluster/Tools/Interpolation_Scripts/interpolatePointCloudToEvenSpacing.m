function [interpolatedPtCloud,F] = interpolatePointCloudToEvenSpacing(ptCloud,spacing,zCutoff)
%%A function to create a scattered interpolant of an input point cloud, and
%%generate interpolated values based on an input spacing
xyz = double(ptCloud.Location);
x = xyz(:,1);
y=xyz(:,2);
z=xyz(:,3);
F = scatteredInterpolant(x,y,z);
F.Method = 'linear';
F.ExtrapolationMethod = 'none';

[Xq,yQ] = meshgrid(min(x):spacing:max(x),min(y):spacing:max(y));
Zq = F(Xq,yQ);

ptCloudData = [Xq(:),yQ(:),Zq(:)];
ptCloudData = ptCloudData(ptCloudData(:,3)<zCutoff,:);

interpolatedPtCloud = pointCloud(ptCloudData);
cmatrix = ones(size(interpolatedPtCloud.Location)).*[1 1 1];
interpolatedPtCloud = pointCloud(ptCloudData,'Color',cmatrix);

end