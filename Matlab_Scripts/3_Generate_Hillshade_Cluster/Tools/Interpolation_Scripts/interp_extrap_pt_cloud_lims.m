function [pt_cloud_interp_extrap,extrap_points,base_topo_points] = interp_extrap_pt_cloud_lims(pt_cloud_interp, base_topography_interp, mask, xLim, yLim, spacing)
%%A function to sample a point cloud within a user defined rectangular
%%area.  Their are two unique aspects of this function.  One is that within the mask a
%%linear interpolation is used, and outside the mask a nearest neighbor
%%extrapolation is used.  The second is that when a point falls below the user specified
%%base topography than the base topography value is given.  This has the
%%effect of the interpolant 'filling' the channel.
%% Inputs:
% F: a scattered interpolant of the target point cloud
% mask: zone within which to interpolate with linear interpolation, defined as a set of x,y points
% base_topography: The channel topography that will be used to determine when the channel is 'filled'.  
% This should be resampled to be the same as the interpolated point cloud
% xlim/ylim: the x and y extents of the resulting interplant
% spacing: the spacing of the interpolant
%% Outputs: 
% pt_cloud_interp_extrap: resulting point cloud
%extrap_points: points that were derived from nearest neighbor
% base_topo_points: points derived from base topography

%%generate sample points
[Xq,Yq] = meshgrid(min(xLim):spacing:max(xLim),min(yLim):spacing:max(yLim));
Xq = Xq(:);
Yq = Yq(:);
Zq = zeros(size(Xq));
%%linear interpolate inside
linear_interp_points = inpolygon(Xq,Yq,mask(:,1),mask(:,2));
pt_cloud_interp.Method = 'linear';
pt_cloud_interp.ExtrapolationMethod = 'nearest';
Zq(linear_interp_points) = pt_cloud_interp(Xq(linear_interp_points),Yq(linear_interp_points));

%%nearest neighbor outside
extrap_points = ~linear_interp_points;
pt_cloud_interp.Method = 'nearest';
pt_cloud_interp.ExtrapolationMethod = 'nearest';
Zq(extrap_points) = pt_cloud_interp(Xq(extrap_points),Yq(extrap_points));

%%keep base topography where 
base_topo_points = base_topography_interp.Location(:,3) > Zq;
Zq(base_topo_points) = base_topography_interp.Location(base_topo_points,3);

pt_cloud_interp_extrap = pointCloud([Xq,Yq,Zq]);
end