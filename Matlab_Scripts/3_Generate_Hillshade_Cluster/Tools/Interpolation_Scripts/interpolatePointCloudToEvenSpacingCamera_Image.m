function [interpolatedPtCloud,F] = interpolatePointCloudToEvenSpacingCamera_Image(ptCloud,spacing,zCutoff,xlim,ylim)
%%A function to create a scattered interpolant of an input point cloud, and
%%generate interpolated values based on an input spacing
xyz = double(ptCloud.Location);
x = xyz(:,1);
y=xyz(:,2);
z=xyz(:,3);
F = scatteredInterpolant(x,y,z);
F.Method = 'linear';
F.ExtrapolationMethod = 'none';

[Xq,yQ] = meshgrid(xlim(1):spacing:xlim(2),ylim(1):spacing:ylim(2));
Zq = F(Xq,yQ);

ptCloudData = cat(3,Xq,yQ,Zq);
% ptCloudData = ptCloudData(ptCloudData(:,3)<zCutoff,:);
% ptCloudData = ptCloudData(ptCloudData(:,2)<0,:);

interpolatedPtCloud = pointCloud(ptCloudData);
% cmatrix = ones(size(interpolatedPtCloud.Location)).*[1 1 1];
% interpolatedPtCloud = pointCloud(ptCloudData,'Color',cmatrix);

end