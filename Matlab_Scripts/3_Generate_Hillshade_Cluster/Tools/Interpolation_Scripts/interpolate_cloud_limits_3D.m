function [interpolatedPtCloud,F,x,y] = interpolate_cloud_limits_3D(ptCloud,spacing,xLim,yLim)
%%A function to create a scattered interpolant of an input point cloud, and
%%generate interpolated values based on an input spacing, and then
%%generate a hillshade
xyz = double(ptCloud.Location);
xyz(isnan(xyz(:,1)),:) = 0;
x = xyz(:,:,1);
y=xyz(:,:,2);
z=xyz(:,:,3);
F = griddedInterpolant(x',y',z');
F.Method = 'linear';
F.ExtrapolationMethod = 'none';

[Xq,yQ] = meshgrid(min(xLim):spacing:max(xLim),min(yLim):spacing:max(yLim));
Zq = F(Xq,yQ);

ptCloudData = cat(3,Xq,yQ,Zq);

interpolatedPtCloud = pointCloud(ptCloudData);
% cmatrix = ones(size(interpolatedPtCloud.Location)).*[1 1 1];
% interpolatedPtCloud = pointCloud(ptCloudData,'Color',cmatrix);
x = [min(xLim):spacing:max(xLim)];
y= [min(yLim):spacing:max(yLim)];


end