function [interpolatedPtCloud,F,x,y] = interpolate_cloud_limits_convex_hull(ptCloud,spacing,xLim,yLim)
%%A function to create a scattered interpolant of an input point cloud, and
%%generate interpolated values based on an input spacing, and then
%%generate a hillshade
xyz = double(ptCloud.Location);
xyz(isnan(xyz(:,1)),:) = 0;
x = xyz(:,1);
y=xyz(:,2);
z=xyz(:,3);
F = scatteredInterpolant(x,y,z);
F.Method = 'linear';
F.ExtrapolationMethod = 'none';

%remove points outside of cloud
k = boundary(x,y,1);

[Xq,yQ] = meshgrid(min(xLim):spacing:max(xLim),min(yLim):spacing:max(yLim));
Zq = F(Xq,yQ);

ptCloudData = [Xq(:),yQ(:),Zq(:)];

in = inpolygon(Xq(:),yQ(:),x(k),y(k));

ptCloudData = ptCloudData(in,:);

interpolatedPtCloud = pointCloud(ptCloudData);
cmatrix = ones(size(interpolatedPtCloud.Location)).*[1 1 1];
interpolatedPtCloud = pointCloud(ptCloudData,'Color',cmatrix);
x = [min(xLim):spacing:max(xLim)];
y= [min(yLim):spacing:max(yLim)];


end

function debug_region_plot(x,y,k)
figure
plot(x,y,'.b')
 hold on
plot(x(k),y(k),'.-r')
axis equal

end