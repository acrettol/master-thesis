function [vel_depth_timetable, error_log_T] = HS_Velocity_PLY_Azi_Alti_Stack_NEW(hs_params,...
    ply_folder,...
    pivlab_settings,...
    image_preproc_settings,...
    image_postproc_settings,...
    dt,...
    timestamps_eT_select)
%{
A function to generate ortho photos, use PIV lab to get 2D displacements and 
then reproject the displacements into 3D.

This function also estiamtes flow depth at the same locations

This function assumes a stationary sensor

Author: Jordan Aaron
Modified: Raffaele Spielmann, 29.2.2024
Modified: Arnaud Crettol, 01.05.2024 (PIVlab post_proc, not working yet)
Modified: Arnaud Crettol, 13.06.2024 (Sensor_box points removal)
%}


%% Load data and define parameters

% Number of points in pointCloud threshold:
threshold_minNrPoints = 10000; % minimum number of points that a point cloud must have; if below, no hillshade (and no PIV vel.) will be generated (to avoid error; and b/c Ouster OS LiDAR should have around 100'000 pts.)

%interpolation settings
remove_sensor = hs_params.remove_sensor;
sensor_box = hs_params.sensor_box; %clip this box out
zMax = hs_params.zMax; %clip below this value
spacing = hs_params.spacing; %ortho photo pixel size
xlim = hs_params.xlim; %ortho photo x extent
ylim = hs_params.ylim; %ortho photo y extent
xMin = sensor_box(1);
xMax = sensor_box(2);
yMin = sensor_box(3);
yMax = sensor_box(4);
zMin = sensor_box(5);
zMax_pt = sensor_box(6);

%hs params
azi_range = hs_params.azi_range;
alti_range = hs_params.alti_range;

%load base cloud
base_pt_cloud = pcread(hs_params.base_pt_cloud);

%% Step 1: If using a DEM for clipping the interpolant, load it in and interpolate it
[base_cloud_interp,base_interp] = interpolate_cloud_limits(base_pt_cloud,spacing,xlim,ylim);
% frames = [623,624]; %temporary to debug

%% Preparation of Frames and Setup

% Define Number of frames and sequence of frames (for parfor)
total_number = hs_params.end_frame - hs_params.start_frame + 1; % total number of frames
increment = ceil(total_number/101);
sequence = [hs_params.start_frame:increment:hs_params.end_frame];
sequence(end) = hs_params.end_frame -1; % last iteration must stop at endFrame-1, because velocities (i.e. diff. between 2 frames) is calculated here!

frames = hs_params.start_frame:hs_params.end_frame; % (actual) frame numbers

% Determine structure (length) of PIV-output velocities (u,v) --> will be used later to assign NaNs (of this dimension) to "faulty" ptClouds
[world_dim] = HS_Velocity_determine_world_dim(hs_params,ply_folder,pivlab_settings,image_preproc_settings,dt,frames); % function call, to check on first 100 frames

% Prepare error log file, where outputed errors / infos etc. are saved per frame
error_log_T = table('Size',[total_number,2], 'VariableTypes',{'double','string'}, 'VariableNames',["FrameNr","ErrorMsg"]);

% Set up cluster / parallel pool
local_job = parcluster('local');
pool = parpool(local_job, hs_params.num_cores);

%% Parallel for loop to create HS photo and get velocities

fprintf('Calculation started now: %s \n', datetime('now'))

iter_time = NaN;

for j = 1:100

    % find (frame) numbers of current sequent and (index) numbers
    cur_sequence = [sequence(j) sequence(j+1)]; % e.g. 4100 - 4200
    indices_cur_seq = find(frames==cur_sequence(1)):find(frames==cur_sequence(2)); % find "index" (i.e. "row number") of a given frame (e.g. 4005) in the "frames" vector, to use this index later to generate timetables, etc.

    % inform user about time to finish
    time_to_finish = seconds(iter_time*(100-j+1));
    time_at_finish = datetime('now')+time_to_finish;
    fprintf('The calculation is: %d%% finished, expected time to finish is: %s \n', j-1, time_at_finish);

    tic

    parfor i_index = indices_cur_seq
            
        % Step 2aI: Load point clouds, and filter with zMax
            % ptCloud1
            ptCloud1 = pcread(fullfile(ply_folder,sprintf('%0.5d.ply',frames(i_index))));
            if remove_sensor == "Yes"
                indices1 = ptCloud1.Location(:,1) < xMin | ptCloud1.Location(:,1) > xMax | ...
                     ptCloud1.Location(:,2) < yMin | ptCloud1.Location(:,2) > yMax | ...
                     ptCloud1.Location(:,3) < zMin | ptCloud1.Location(:,3) > zMax_pt;
            elseif remove_sensor == "No"
                indices1 = ptCloud1.Location(:,3) < zMax;
            end
            ptCloud1 = select(ptCloud1,indices1);

            % ptCloud2
            ptCloud2 = pcread(fullfile(ply_folder,sprintf('%0.5d.ply',frames(i_index+1))));
            if remove_sensor == "Yes"
                indices2 = ptCloud2.Location(:,1) < xMin | ptCloud2.Location(:,1) > xMax | ...
                     ptCloud2.Location(:,2) < yMin | ptCloud2.Location(:,2) > yMax | ...
                     ptCloud2.Location(:,3) < zMin | ptCloud2.Location(:,3) > zMax_pt;
            elseif remove_sensor == "No"
                indices2 = ptCloud2.Location(:,3) < zMax;
            end
            ptCloud2 = select(ptCloud2,indices2);

        % Step 2aII: Check if point clouds are "good", i.e. not empty (otherwise Hillshade interpolation will break) -----------------------------

        if isempty(ptCloud1.Location) || numel(nonzeros(ptCloud1.Location)) < threshold_minNrPoints % if NO points in ptCloud1 OR only a few (e.g. 20'000 points) in ptCloud1

            % set to NaN   --> use same dimensions as output from PIV (determined previously in determine_world_dim function)
                world_coords{i_index} = NaN(world_dim, 3);
                world_vel{i_index} = NaN(world_dim, 3);
                world_flow_depth{i_index} = NaN(world_dim, 1);

            % save Info to error log:
            error_log_T(i_index,:) = {frames(i_index), sprintf("Info: Empty ptCloud: No Hillshade velocities for Frame = %d (1st ptCloud)", frames(i_index))};


        elseif isempty(ptCloud2.Location) || numel(nonzeros(ptCloud2.Location)) < threshold_minNrPoints % if no points in second point cloud (no PIV correlation possible)

            % set to NaN
                world_coords{i_index} = NaN(world_dim, 3);
                world_vel{i_index} = NaN(world_dim, 3);
                world_flow_depth{i_index} = NaN(world_dim, 1);

            % save Info to error log:
            error_log_T(i_index,:) = {frames(i_index), sprintf("Info: Empty ptCloud: No Hillshade velocities for Frame = %d (2st ptCloud)", frames(i_index+1))};


        elseif dt(i_index)<0 || dt(i_index)>0.5 % if timestamp-difference (dt) is unreasonable (negative) or too big (missing frames, more than 0.5 s)

            % set to NaN
                world_coords{i_index} = NaN(world_dim, 3);
                world_vel{i_index} = NaN(world_dim, 3);
                world_flow_depth{i_index} = NaN(world_dim, 1);

            % save Info to error log:
            error_log_T(i_index,:) = {frames(i_index), sprintf("Info: Faulty Timestamp dt: No Hillshade velocities for Frame = %d", frames(i_index))};

        % --------------------------------------------------------------------------------------------------------------- end of check -----------

        else % if not empty --> actually calculate hillshade PIV velocity (if possible, i.e. if not any other error --> try/catch)

            % TRY: calculating hillshade velocities
            % --> assuming no error occurs, because problems have been filtered in if/elseif above
            % --> in case there is any other error, script will NOT break, but continue and pass error to CATCH, and then cont. with next iteration
            try

                % Generate DEM (interpolation), used for hillshade later
                [DEM_1,y_range_1,x_range_1,interp1] = get_DEM_for_Hillshade(ptCloud1,spacing,xlim,ylim);
                [DEM_2,y_range_2,x_range_2,~] = get_DEM_for_Hillshade(ptCloud2,spacing,xlim,ylim);
        
                % Generate hillshades for all azimuth and altidue combinations, and get the PIV velocities
                num_combos = 0;
                u=[];
                v=[]; 
                for k = 1:length(azi_range)
                    for z = 1:length(alti_range)
                        % Generate Hillshades
                        HS_1 = hillshade_esri_azi_alt(DEM_1,y_range_1,x_range_1,'azimuth',azi_range(k),'altitude',alti_range(z));
                        HS_img_1 = mat2gray(HS_1);
                        HS_2 = hillshade_esri_azi_alt(DEM_2,y_range_2,x_range_2,'azimuth',azi_range(k),'altitude',alti_range(z));
                        HS_img_2 = mat2gray(HS_2);
        
                        % Get PIV velocities (the code below is based on the pivlab documentation)
                        image1 = PIVlab_preproc (HS_img_1,image_preproc_settings{1,2},image_preproc_settings{2,2},image_preproc_settings{3,2},image_preproc_settings{4,2},image_preproc_settings{5,2},image_preproc_settings{6,2},image_preproc_settings{7,2},image_preproc_settings{8,2},image_preproc_settings{9,2},image_preproc_settings{10,2}); %preprocess images
                        image2 = PIVlab_preproc (HS_img_2,image_preproc_settings{1,2},image_preproc_settings{2,2},image_preproc_settings{3,2},image_preproc_settings{4,2},image_preproc_settings{5,2},image_preproc_settings{6,2},image_preproc_settings{7,2},image_preproc_settings{8,2},image_preproc_settings{9,2},image_preproc_settings{10,2});
                        
                        %FFT Multi-Pass Algorithm
                        [x, y, u(:,:,num_combos+1), v(:,:,num_combos+1), ~,~,~] = piv_FFTmulti(image1,image2,pivlab_settings{1,2},pivlab_settings{2,2},pivlab_settings{3,2},pivlab_settings{4,2},pivlab_settings{5,2},pivlab_settings{6,2},pivlab_settings{7,2},pivlab_settings{8,2},pivlab_settings{9,2},pivlab_settings{10,2},pivlab_settings{11,2},pivlab_settings{12,2},pivlab_settings{13,2},0,pivlab_settings{14,2},pivlab_settings{15,2});
                        
                        %Post Processing: does it take single values as input or arrays like the output of piv_FFTmulti ? 
                        % [u(:,:,num_combos+1),v(:,:,num_combos+1)] = PIVlab_postproc (u(:,:,num_combos+1),v(:,:,num_combos+1),image_postproc_settings{1,2},image_postproc_settings{2,2}, image_postproc_settings{3,2}, image_postproc_settings{4,2},image_postproc_settings{5,2},image_postproc_settings{6,2},image_postproc_settings{7,2})
                        num_combos = num_combos+1;
                    end
                end
                % mean velocities from all combinations
                u = mean(u,3); %./num_combos;
                v = mean(v,3); %./num_combos;
    
                % Project pixel displacements from 2D to 3D and get 3D velocities
        
                % convert pixel to world coordinates
                world_coords_iter = [xlim(1)+x(:)*spacing-spacing, ylim(1)+y(:)*spacing-spacing];
                world_vel_iter = [u(:)./dt(i_index).*spacing, v(:)./dt(i_index).*spacing];
        
                % project 2D displacements to 3D
                interp_coords = double(world_coords_iter);
                z_coord_base = interp1(interp_coords);
                z_coord_disp = interp1(double(world_coords_iter)+double(world_vel_iter.*dt(i_index)));
                z_displacement = z_coord_disp - z_coord_base;
        
                % save the 3D results
                world_coords{i_index} = [world_coords_iter,z_coord_base];
                world_vel{i_index} = [world_vel_iter,z_displacement./dt(i_index)];
    
                % get flow depth
                world_flow_depth{i_index} = interp1(double(world_coords_iter))-base_interp(double(world_coords_iter));
    
                % add "no error" to error log (because it should have worked)
                error_log_T(i_index,:) = {frames(i_index), missing}; % missing = empty string

            % CATCH: catch error message from Matlab
            catch ME

                % Output Matlab error message to error log
                error_log_T(i_index,:) = {frames(i_index), sprintf("ERROR: Frame = %d; Matlab Error: %s", frames(i_index), ME.message)};

                % Also output to command line (slurm file):
                fprintf("ERROR Warning: Frame = %d; Matlab Error (see below) \n", frames(i_index)) 
                fprintf("-> Message: %s \n", ME.message) % output error message
                
                for iii = 1:length(ME.stack) % if caused by different functions within functions, errors will be "stacked"
                    fprintf("-> Cause: Name = %s, Line = %d \n",ME.stack(iii).name, ME.stack(iii).line) % output cause of error (functions, etc.)
                end

                % set to NaN (as above; to avoid empty):
                    world_coords{i_index} = NaN(world_dim, 3);
                    world_vel{i_index} = NaN(world_dim, 3);
                    world_flow_depth{i_index} = NaN(world_dim, 1);

                % script will continue with next iteration after this

            end % end of "try/catch"
    
        end % end of "if"

    end % end of "parfor"

    iter_time = toc;

end % end of "for"


%% Concatenate to Timetable

% omit last value of selected timestamps (because velocities)

timestamps_eT_select(end) = []; % omit last value

Time = seconds(timestamps_eT_select);
Time = duration(Time, 'Format', 'mm:ss.SS');

vel_depth_timetable = timetable(Time,...
    world_coords',...
    world_vel',...
    world_flow_depth',...
    'VariableNames',...
    {'world_coords_xyz','world_velocity_xyz','world_flow_depth'});



% CHECK this (ask JORDAN)!

% t = seconds([0 cumsum(dt)]);
% vel_datetime = datetime('now')+t;
% vel_datetime = vel_datetime-vel_datetime(1);
% 
% vel_depth_timetable = timetable(vel_datetime(1:numFrames-1),...
%     world_coords',...
%     world_vel',...
%     world_flow_depth',...
%     'VariableNames',...
%     {'world_coords_xyz','world_velocity_xyz','world_flow_depth'});

pool.delete();

fprintf('Calculation finished now: %s \n', datetime('now'))

end % end of "function"


