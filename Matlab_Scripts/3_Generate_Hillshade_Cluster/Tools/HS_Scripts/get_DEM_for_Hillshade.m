function [Zq,y_range,x_range,F] = get_DEM_for_Hillshade(ptCloud,spacing,xLim,yLim)
%%A function to create a scattered interpolant of an input point cloud, and
%%generate interpolated values based on an input spacing, and then
%%generate a hillshade

xyz = double(ptCloud.Location);
xyz(isnan(xyz(:,1)),:) = 0;
x = xyz(:,1);
y=xyz(:,2);
z=xyz(:,3);
F = scatteredInterpolant(x,y,z);
F.Method = 'linear';
F.ExtrapolationMethod = 'none';

[Xq,yQ] = meshgrid(min(xLim):spacing:max(xLim),min(yLim):spacing:max(yLim));
Zq = F(Xq,yQ);
x = [min(xLim):spacing:max(xLim)];
y= [min(yLim):spacing:max(yLim)];
y_range = [min(y):spacing:max(y)];
x_range= [min(x):spacing:max(x)];


end