function [H,x,y, interpolatedPtCloud,F] = createScanHillshade_Filter(ptCloud,spacing,xLim,yLim)
%%A function to create a scattered interpolant of an input point cloud, and
%%generate interpolated values based on an input spacing, and then
%%generate a hillshade
xyz = double(ptCloud.Location);
xyz(isnan(xyz(:,1)),:) = 0;
x = xyz(:,1);
y=xyz(:,2);
z=xyz(:,3);
F = scatteredInterpolant(x,y,z);
F.Method = 'linear';
F.ExtrapolationMethod = 'none';

[Xq,yQ] = meshgrid(min(xLim):spacing:max(xLim),min(yLim):spacing:max(yLim));
Zq = F(Xq,yQ);
ptCloudData = [Xq(:),yQ(:),Zq(:)];
K = (1/9)*ones(3);

for i = 1:10
ptCloudData = conv2(ptCloudData,K,'same'); 
end
interpolatedPtCloud = pointCloud(ptCloudData);
cmatrix = ones(size(interpolatedPtCloud.Location)).*[1 1 1];
interpolatedPtCloud = pointCloud(ptCloudData,'Color',cmatrix);
x = [min(xLim):spacing:max(xLim)];
y= [min(yLim):spacing:max(yLim)];
H = hillshade_esri(Zq,[min(y):spacing:max(y)],[min(x):spacing:max(x)]);

end