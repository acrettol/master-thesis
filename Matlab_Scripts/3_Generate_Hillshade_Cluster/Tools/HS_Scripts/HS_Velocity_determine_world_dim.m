function [world_dim] = HS_Velocity_determine_world_dim(hs_params,...
    ply_folder,...
    pivlab_settings,...
    image_preproc_settings,...
    dt,...
    frames)


%interpolation settings
zMax = hs_params.zMax; %clip below this value
spacing = hs_params.spacing; %ortho photo pixel size
xlim = hs_params.xlim; %ortho photo x extent
ylim = hs_params.ylim; %ortho photo y extent

%hs params
azi_range = hs_params.azi_range;
alti_range = hs_params.alti_range;


% for loop (to test first 100 frames, in case first few ones are incomplete / broken), to get dimension of world_coords, etc.

for ii = 1:100

            % ptCloud1
            ptCloud1 = pcread(fullfile(ply_folder,sprintf('%0.5d.ply',frames(ii))));
            indices = ptCloud1.Location(:,3) < zMax;
            ptCloud1 = select(ptCloud1,indices);
        
            % ptCloud2
            ptCloud2 = pcread(fullfile(ply_folder,sprintf('%0.5d.ply',frames(ii+1))));
            indices = ptCloud2.Location(:,3) < zMax;
            ptCloud2 = select(ptCloud2,indices);

        % Step 2aII: Check if point clouds are "good", i.e. not empty (otherwise Hillshade interpolation will break) -----------------------------

        if isempty(ptCloud1.Location) || numel(nonzeros(ptCloud1.Location)) < 20000 % if NO points in ptCloud1 OR only a few (i.e. 10'000 points) in ptCloud1
    
            continue % go to next iteration, i.e. next frame

        elseif isempty(ptCloud2.Location) || numel(nonzeros(ptCloud2.Location)) < 20000 % if no points in second point cloud (no PIV correlation possible)
            
            continue

        elseif dt(ii)<0 || dt(ii)>0.5 % if timestamp-difference (dt) is unreasonable (negative) or too big (missing frames, more than 0.5 s)

            continue

        % --------------------------------------------------------------------------------------------------------------- end of check -----------

        else % if not empty --> actually calculate hillshade PIV velocity

            % Generate DEM (interpolation), used for hillshade later
            [DEM_1,y_range_1,x_range_1,interp1] = get_DEM_for_Hillshade(ptCloud1,spacing,xlim,ylim);
            [DEM_2,y_range_2,x_range_2,~] = get_DEM_for_Hillshade(ptCloud2,spacing,xlim,ylim);
    
            % Generate hillshades for all azimuth and altidue combinations, and get the PIV velocities
            num_combos = 0;
            u=[];
            v=[]; 
            for k = 1:length(azi_range)
                for z = 1:length(alti_range)
                    % Generate Hillshades
                    HS_1 = hillshade_esri_azi_alt(DEM_1,y_range_1,x_range_1,'azimuth',azi_range(k),'altitude',alti_range(z));
                    HS_img_1 = mat2gray(HS_1);
                    HS_2 = hillshade_esri_azi_alt(DEM_2,y_range_2,x_range_2,'azimuth',azi_range(k),'altitude',alti_range(z));
                    HS_img_2 = mat2gray(HS_2);
    
                    % Get PIV velocities (the code below is based on the pivlab documentation)
                    image1 = PIVlab_preproc (HS_img_1,image_preproc_settings{1,2},image_preproc_settings{2,2},image_preproc_settings{3,2},image_preproc_settings{4,2},image_preproc_settings{5,2},image_preproc_settings{6,2},image_preproc_settings{7,2},image_preproc_settings{8,2},image_preproc_settings{9,2},image_preproc_settings{10,2}); %preprocess images
                    image2 = PIVlab_preproc (HS_img_2,image_preproc_settings{1,2},image_preproc_settings{2,2},image_preproc_settings{3,2},image_preproc_settings{4,2},image_preproc_settings{5,2},image_preproc_settings{6,2},image_preproc_settings{7,2},image_preproc_settings{8,2},image_preproc_settings{9,2},image_preproc_settings{10,2});
    
                    [x, y, u(:,:,num_combos+1), v(:,:,num_combos+1), ~,~,~] = piv_FFTmulti(image1,image2,pivlab_settings{1,2},pivlab_settings{2,2},pivlab_settings{3,2},pivlab_settings{4,2},pivlab_settings{5,2},pivlab_settings{6,2},pivlab_settings{7,2},pivlab_settings{8,2},pivlab_settings{9,2},pivlab_settings{10,2},pivlab_settings{11,2},pivlab_settings{12,2},pivlab_settings{13,2},0,pivlab_settings{14,2},pivlab_settings{15,2});
                    num_combos = num_combos+1;
                end
            end
            % mean velocities from all combinations
            u = mean(u,3); %./num_combos;
            v = mean(v,3); %./num_combos;

            break % --> if "good" point cloud could be found, exit for-loop, to get dimension of u / v 
    
        end % end of "if"

end % end of "for"


% determine dimensions of u / v matrices, which define the length of word_coords, world_vel, etc.
world_dim = length(u) * width(u);

% save to world_dim and export as function output (needed for Hillshade main function)

end