% 25.10.2024
% Jordan Aaron, modified Raffaele Spielmann and then by Arnaud Crettol

% Script to Digitize the Boundary for the Velocity Field Extraction
% NOTE: need to run "get_HS_velocity" on cluster, and then download output (unsmoothed velocity data) to Tiliva (or locally)
clc
clear all
close all


addpath(genpath('Tools'));

%% Load Configuration File and Select Parameters

% Get Sensor Name and Date --> ask user
% ------------------------------------------------ UI ---
prompt      = {'Enter Catchment (e.g. SPR, ILL,...)','Enter Location (e.g. Gazo, Owen, ...):','Enter Year of event (e.g. 2023):', 'Enter date of Data collection (e.g. 2023_06_30):', 'Enter Nr. of frame to be plotted to draw boundary on:'};
dlgtitle    = 'Input';
fieldsize   = [1 45; 1 45; 1 45; 1 45; 1 45];
definput    = {'ILL','Gazo','2023','2023_07_13','10'};
answer      = inputdlg(prompt,dlgtitle,fieldsize,definput);
% ------------------------------------------------ UI ---
loc_name    = answer{1};             % e.g. "ILL", "GAD", ...
sensor_name = answer{2};             % e.g. 'Owen', 'Bloom', ...
event_year  = answer{3};             % e.g. "2023"
event_date  = answer{4};             % e.g. '2022_06_03'
frameNr     = str2double(answer{5}); % frame of point cloud which will be selected and projected as Hillshade

% Load configuration file used on Cluster
config_filename = fullfile('Configuration_Files',loc_name,event_year,event_date,sprintf('%s_%s_config_HS.mat',sensor_name, event_date)); % name of config file
load(config_filename)

% Extraction of parameters from Config. file:
xLim    = hs_params.xlim;    % xLimits --> take same as you did for generating Hillshades!
yLim    = hs_params.ylim;    % yLimits of HS
spacing = hs_params.spacing; % m, Hillshade resolution
zMax    = hs_params.zMax;    %clip below this value

% Hillshade parameters (just used for plotting here)
azimuth  = 90;
altitude = 45;


%% Load data

% Load Velocity Data (unsmoothed)
[velFilename, velPath] = uigetfile('*.mat', 'Select a VELOCITY DATASET (hillshade, etc.)');
disp('Loading Velocity Dataset ... (might take some minutes) ...')
vel_data = load(fullfile(velPath, velFilename)); % load velocity dataset (e.g. "HS_vel_azi_alti_cluster_res05.mat")
%%
% Load One Point Cloud (.ply file)
disp('Selecting (corresponding) PLY files...')
ptCloudPLYpath = uigetdir('E:\Data\', 'Select folder with PLY files'); % e.g. 'E:\01_Projects\01_Ambizione\UncompressData\SPR\Events\2023\2023_08_24\DATA\02_PreProcessedData\ptClouds_ply_rot';
currFrame      = pcread(fullfile(ptCloudPLYpath,sprintf('%0.5d.ply',frameNr)));
indices        = currFrame.Location(:,3) < 4;
currFrame      = select(currFrame,indices);


%% Processing

% create 1 hillshade (of frame = frameNr)
[H,xRange,yRange, interpolatedPtCloud,F] = createScanHillshade_Azimuth_Elevation(currFrame,spacing,xLim,yLim,azimuth,altitude);
frame = mat2gray(H);

R = imref2d(size(frame),xLim,yLim);
spacing2 = 1;
[Xq,yQ]  = meshgrid(min(xLim):spacing2:max(xLim),min(yLim):spacing2:max(yLim));

% plot hillshdade
imshow(frame,R)
hold on

% extract position (coord.) and velocities of frame = frameNr
pos = vel_data.vel_depth_TT.world_coords_xyz{frameNr}; % --> CHANGE later (for cases where .ply files do not start at 00001.ply (but e.g. 019000.ply)
vel = vel_data.vel_depth_TT.world_velocity_xyz{frameNr};

% vector plot velocity vector field (on top of HS)
quiver(pos(:,1),pos(:,2),vel(:,1),vel(:,2));

% ask user to select "relevant" area
disp('Select relevant area / boundary of velocity vector field.')
roi = drawpolygon('Color','r');
boundary = roi.Position;

%% Saving

% Define folder where to save "boundary":
boundary_save_folder = fullfile('B_Smoothing_Boundary_Files',loc_name,event_year,event_date); % define folder path

% Create folders if they do not exist:
if ~exist(boundary_save_folder, 'dir')
    mkdir(boundary_save_folder);
    fprintf('Created Folder: %s \n', boundary_save_folder)
end

% save "boundary"
save(fullfile(boundary_save_folder, sprintf('Boundary_%s_%s.mat', sensor_name, event_date)), 'boundary');

% inform user
fprintf('+++ Boundary File Saved +++ \n File and Path: Boundary_%s_%s.mat in %s \n +++ +++ \n', sensor_name, event_date, boundary_save_folder)