function C_Get_HS_velocity(configuration_file)
%
% Function to get Hillshade velocities, based on a range of azimuth/altitude values
% input: previously generated "configuration_file", from "generate_configuration_file_timestamp" script, named "SensorName_Date_config_HS.mat"
% Arnaud 10.05.2024: added post processing in PIVlab (not useful/not working)

addpath(genpath('Tools'),genpath('PIVlab'));

load(configuration_file); % load config_HS file
                          % --> contains folder_paths, hs_params, p & s (PIV Settings)

%% Determine Number of Frames

% Comment out, if using hs_params.start_frame and .end_frame, which can be defined in "generate_configuration_file..."

% get filenames and number of files (in .ply folder)
files = dir([folder_paths.ply_folder '/*.ply']);
numFrames = numel(files); %number of ply files to load
[~, NrFirstFrame, ~] = fileparts(files(1).name); % get name (number) of first frame
% start_index = folder_paths.start_index;% str2double(NrFirstFrame); % convert to double and save
start_frame = str2double(NrFirstFrame); % convert to double and save
end_frame = start_frame + numFrames -1;

hs_params.start_frame = start_frame;
hs_params.end_frame = end_frame;

%% Check if files and paths exist

% Check if folders and files exist, save result in vector (= 0, if non-existing)
pathCheck(1,1) = exist(folder_paths.ply_folder, 'dir');
pathCheck(1,2) = exist(folder_paths.timestamp_file, 'file');
pathCheck(1,3) = exist(folder_paths.output_folder, 'dir');

if all(pathCheck) % if all elements in pathCheck are non-zeros (i.e. exist)

    fprintf('+++ Path Check successfull+++ \n') % inform user if sucessfull

else

    fprintf('+++ Path Check NOT successfull +++ \n') % inform user if not successfull
    fprintf('PLY folder = %s; exists = %s \n', folder_paths.ply_folder, mat2str(all(pathCheck(1,1))))
    fprintf('Timestamp file = %s; exists = %s \n', folder_paths.timestamp_file, mat2str(all(pathCheck(1,2))))
    fprintf('Output folder = %s; exists = %s \n', folder_paths.output_folder, mat2str(all(pathCheck(1,3))))

    error('ERROR: not all paths and files defined!') % break program
end


%% Get Timestamp information anc calculate "real" timestamps

timestamps = readtable(folder_paths.timestamp_file); % predefined path to timestamp.txt file
timestamps_eventTime = timestamps.Time - timestamps.Time(1); % convert (absolute) timestampts to (relative) event time timestamps (frame1 = 00:00:00)
timestamps_eT_select = timestamps_eventTime(hs_params.start_frame:hs_params.end_frame); % select relevant timestamps
dt = diff(timestamps_eT_select); % calculate difference between "real" timestamps
% dt = ones(numFrames ,1)*0.1; % keep as an array in case something more clever is done in the future, like using 'real' time measurement from the sensor


%% Call Function to get Hillshade Velocities


[vel_depth_TT, error_log_T] = HS_Velocity_PLY_Azi_Alti_Stack_NEW(hs_params,...
    folder_paths.ply_folder,...
    s,...
    p,...
    q,...
    dt,...
    timestamps_eT_select);

% Save velocities
save(fullfile(folder_paths.output_folder, sprintf('HS_vel_azi_alti_res%03.0f.mat', hs_params.spacing*1000)), 'vel_depth_TT', '-v7.3');
% save, with resolution included in filename

% Save error log as .txt
writetable(error_log_T, fullfile(folder_paths.output_folder, sprintf('HS_vel_ErrorLog_res%03.0f.txt', hs_params.spacing*1000)))


disp('+++ Variables Saved +++ END');

end