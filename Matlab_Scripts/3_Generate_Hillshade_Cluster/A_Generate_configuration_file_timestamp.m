% 25.10.2024
% Jordan Aaron, modified Raffaele Spielmann and then by Arnaud Crettol

% Script to create "configuration file" for Hillshade Velocities 
clc
clear all
close all


%% Define Names and Path

folder_paths = struct();

% Define Sensor Name and Date --> ask user
% ------------------------------------------------ UI ---
prompt    = {'Enter Station (e.g. SPR, ILL,...)','Enter Location (e.g. Gazoduc, CD29, CD27, ...):','Enter Sensor (e.g. Gazo, Owen, ...):','Enter Year of event (e.g. 2023):', 'Enter date of Data collection (e.g. 2023_06_30):'};
dlgtitle  = 'Input';
fieldsize = [1 45; 1 45; 1 45; 1 45; 1 45];
definput  = {'ILL','Gazoduc','Gazo','2023','2023_07_12'};
answer    = inputdlg(prompt,dlgtitle,fieldsize,definput);
% ------------------------------------------------ UI ---

catch_name  = answer{1};     % ILL, GAD
loc_name    = answer{2};     % Gazoduc, CD29, CD27
sensor_name = answer{3};     % Gazo, Bloom, Pip, Owen, Marcel, Merged
event_year  = answer{4};
event_date  = answer{5}; 

% Define Folder Paths
folder_paths.ply_folder     = sprintf('/cluster/scratch/acrettol/Data/%s/%s/PLY_files/%s_ply/',event_date,loc_name,sensor_name);
folder_paths.output_folder  = sprintf('/cluster/scratch/acrettol/Data/%s/%s/Hillshades/%s_5cm_cluster/',event_date,loc_name,sensor_name);
folder_paths.timestamp_file = sprintf('/cluster/scratch/acrettol/Data/%s/%s/Hillshades/%s_timestamps.txt',event_date,loc_name,sensor_name); %for others
%folder_paths.timestamp_file = sprintf('/cluster/scratch/acrettol/Data/%s/%s/Hillshades/Marcel_timestamps.txt',event_date,loc_name); %for CD27 Merged 2023

config_file_save_folder      = fullfile('Configuration_Files',catch_name,event_year,event_date); % folder to save config file
config_filename              = fullfile(config_file_save_folder,sprintf('%s_%s_config_HS',sensor_name, event_date)); % name of config file
folder_paths.config_filename = config_filename; 

% Create folders if they do not exist:
if ~exist(config_file_save_folder, 'dir')
    mkdir(config_file_save_folder);
    fprintf('Created Folder: %s \n', config_file_save_folder)
end


%% Define Hillshade Settings

hs_params = struct();

% Define Hillshade Settings --> CHANGE!!!

%interpolation settings
hs_params.zMax          = 10;     
hs_params.spacing       = 0.05;         
hs_params.xlim          = [-13 12];
hs_params.ylim          = [-8 45];     
hs_params.base_pt_cloud = strcat(folder_paths.ply_folder, '00023.ply'); % 
hs_params.num_cores     = 82;  % up to 128,

hs_params.azi        = 90;
hs_params.alti       = 45;
hs_params.testIndex  = 3000;
hs_params.azi_range  = [90:1:90]; % worse results with a range >> use 90
hs_params.alti_range = [45:1:45]; % worse results with a range >> use 45

% Define first and last frames
hs_params.start_frame = 1;      
hs_params.end_frame   = 74305;

% define a box which contains the sensor (and cables evt.) to exclude them
% from the point cloud and then from the projection into a Hillshade.
% Added by Arnaud Crettol, June 2024
hs_params.remove_sensor = "Yes";
hs_params.sensor_box  = [-0.5 5 -2 0.5 -1 1]; % [xMin xMax yMin yMax zMin zMax]


%% Define Smoothing (Golay) Settings: improved and tested

smoothing_params = struct();

% Define Filter Settings
smoothing_params.sgolay_framelen  = 15;   % frame length for golay filter (sgolayfilt)
smoothing_params.sgolay_polyorder = 3;    % polynomial order for golay filter (must be smaller than framelen) (standard: 3)
smoothing_params.movmean_winlen   = 45;   % window length k of moving mean filter (standrd: 5; i.e. 0.5 s)

% Define path to "boundary" file
smoothing_params.boundary_file = sprintf('B_Smoothing_Boundary_Files/%s/%s/%s/Boundary_%s_%s.mat', catch_name, event_year, event_date, sensor_name, event_date); % use forward slashes

% Path to velocity file (output from get_HS_velocity)
smoothing_params.velocity_file = sprintf('%sHS_vel_azi_alti_res%03.0f.mat', folder_paths.output_folder, hs_params.spacing*1000);



%% Define PIV lab settings (optimized)

% Standard PIV Settings From PIVLab documentation
s = cell(15,2); % To make it more readable, let's create a "settings table"
%Parameter                          %Setting             %Options
s{1,1} = 'Int. area 1';             s{1,2} = 70;         % window size of first pass (44)
s{2,1} = 'Step size 1';             s{2,2} = 35;         % step of first pass
s{3,1} = 'Subpix. finder';          s{3,2} = 1;          % 1 = 3point Gauss, 2 = 2D Gauss
s{4,1} = 'Mask';                    s{4,2} = [];         % If needed, generate via: imagesc(image); [temp,Mask{1,1},Mask{1,2}]=roipoly;
s{5,1} = 'ROI';                     s{5,2} = [];         % Region of interest: [x,y,width,height] in pixels, may be left empty
s{6,1} = 'Nr. of passes';           s{6,2} = 3;          % 1-4 nr. of passes
s{7,1} = 'Int. area 2';             s{7,2} = 35;         % second pass window size
s{8,1} = 'Int. area 3';             s{8,2} = 18;         % third pass window size
s{9,1} = 'Int. area 4';             s{9,2} = 9;          % fourth pass window size
s{10,1}= 'Window deformation';      s{10,2}= '*spline';  % '*spline' is more accurate, but slower
s{11,1}= 'Repeated Correlation';    s{11,2}= 0;          % 0 or 1 : Repeat the correlation four times and multiply the correlation matrices.
s{12,1}= 'Disable Autocorrelation'; s{12,2}= 0;          % 0 or 1 : Disable Autocorrelation in the first pass. 
s{13,1}= 'Correlation style';       s{13,2}= 1;          % 0 or 1 : Use circular correlation (0) or linear correlation (1).
s{14,1}= 'Repeat last pass';        s{14,2}= 0;          % 0 or 1 : Repeat the last pass of a multipass analyis
s{15,1}= 'Last pass quality slope'; s{15,2}= 0.025;      % Repetitions of last pass will stop when the average difference to the previous pass is less than this number.

% Standard image preprocessing settings
p = cell(10,2);
%Parameter                        %Setting             % Options
p{1,1} = 'ROI';                   p{1,2} = s{5,2};     % same as in PIV settings
p{2,1} = 'CLAHE';                 p{2,2} = 1;          % 1 = enable CLAHE (contrast enhancement), 0 = disable
p{3,1} = 'CLAHE size';            p{3,2} = 16;         % CLAHE window size
p{4,1} = 'Highpass';              p{4,2} = 0;          % 1 = enable highpass, 0 = disable
p{5,1} = 'Highpass size';         p{5,2} = 15;         % highpass size
p{6,1} = 'Clipping';              p{6,2} = 0;          % 1 = enable clipping, 0 = disable
p{7,1} = 'Wiener';                p{7,2} = 0;          % 1 = enable Wiener2 adaptive denaoise filter, 0 = disable
p{8,1} = 'Wiener size';           p{8,2} = 3;          % Wiener2 window size
p{9,1} = 'Minimum intensity';     p{9,2} = 0.0;        % Minimum intensity of input image (0 = no change) 
p{10,1}= 'Maximum intensity';     p{10,2}= 1.0;        % Maximum intensity on input image (1 = no change)

% Standard image postprocessing settings (not working, but has little to no effect)
q = cell(7,2);
%Parameter                      %Setting             %Options
%q{1,1} = 'u';                   q{1,2} = 0;         % u(:,:,num_combos+1) in HS_Velocity_PLY_Azi_Alti_Stack_NEW line 157
%q{2,1} = 'v';                   q{2,2} = 0;         % v(:,:,num_combos+1) in HS_Velocity_PLY_Azi_Alti_Stack_NEW line 157
q{1,1} = 'calu';                q{1,2} = -0.5;       % x offsets based on spacing (x increase towards right <=> calu>0)
q{2,1} = 'calv';                q{2,2} = 0.5;        % y offsets based on spacing (y increase towards bottom <=> calv>0)
q{3,1} = 'valid_vel';           q{3,2} = [-8 8 -15 0.01];          % [umin umax vmin vmax]
q{4,1} = 'STDEV check';         q{4,2} = 0;          % 1 = enable Standard deviation filter, 0 = disable
q{5,1} = 'std_thres';           q{5,2} = 5;          % Standard deviation filter threshold
q{6,1} = 'Median check';        q{6,2} = 0;          % 1 = enable local median filter, 0 = disable
q{7,1} = 'neigh_thresh';        q{7,2} = 8;          % local median filter threshold


%% Save CONFIG Files

output_json(config_filename,folder_paths,hs_params,s,p,q);

save(sprintf('%s.mat',config_filename), 'folder_paths','hs_params', 'smoothing_params', 's','p','q'); % save config .mat File

%% Functions

function output_json(output_filename,folder_paths,hs_params,s,p,q)

folder_paths_encode = jsonencode(folder_paths,PrettyPrint=true);
HS_encode = jsonencode(hs_params,PrettyPrint=true);
s_encode = jsonencode(s,PrettyPrint=true);
p_encode = jsonencode(p,PrettyPrint=true);
q_encode = jsonencode(q,PrettyPrint=true);

filename = sprintf('%s.json',output_filename);  % better to use fullfile(path,name) 
fid = fopen(filename,'w');                      % open file for writing (overwrite if necessary)
fprintf(fid,'%s','folder_paths');               % Write the char array, interpret newline as new line
fprintf(fid,'%s',folder_paths_encode);          % Write the char array, interpret newline as new line
fprintf(fid,'\n');                              % Write the char array, interpret newline as new line

fprintf(fid,'%s','HS_Params');        % Write the char array, interpret newline as new line
fprintf(fid,'%s',HS_encode);          % Write the char array, interpret newline as new line
fprintf(fid,'\n');                    % Write the char array, interpret newline as new line

fprintf(fid,'%s','s');          % Write the char array, interpret newline as new line
fprintf(fid,'%s',s_encode);     % Write the char array, interpret newline as new line
fprintf(fid,'\n');              % Write the char array, interpret newline as new line

fprintf(fid,'%s','p');          % Write the char array, interpret newline as new line
fprintf(fid,'%s',p_encode);     % Write the char array, interpret newline as new line
fprintf(fid,'\n');              % Write the char array, interpret newline as new line

fprintf(fid,'%s','q');          % Write the char array, interpret newline as new line
fprintf(fid,'%s',q_encode);     % Write the char array, interpret newline as new line
fprintf(fid,'\n');              % Write the char array, interpret newline as new line

fclose(fid);                    % Close the file (important)

end

% 