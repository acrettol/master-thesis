function D_Smooth_HS_velocity(configuration_file)
% Function to smooth velocity data from "get_HS_velocity"
% input: "configuration_file
% Jordan Aaron, mod. Raffaele Spielmann, 8.3.2024

addpath(genpath('Tools'),genpath('PIVlab'));

load(configuration_file); % load config_HS file

%% Load Configuration and Parameters

% extract parameters from config file
framelen    = smoothing_params.sgolay_framelen;
polyorder   = smoothing_params.sgolay_polyorder;
winlen      = smoothing_params.movmean_winlen;

test_index  = 100; % random frame number, to extract size of array (hard coded as 100, i.e. 100th frame)

% load boundary file (omit vectors outside boundary):
boundary = load(smoothing_params.boundary_file); % pre-defined boundary file (extend of where velocities should be analyzed)

% inform user
fprintf('Loading velocity data from: %s \n', smoothing_params.velocity_file); 
vel_data = load(smoothing_params.velocity_file); % velocity data output from "get_HS_velocity"


%% Pre Pre-Processing

% Find relevant lenghts and sizes of dataset to be smoothed:
[num_frames,~]       = size(vel_data.vel_depth_TT); % find number of frames of Timetable
[num_pts_perFrame,d] = size(vel_data.vel_depth_TT.world_velocity_xyz{test_index}); % find number of points per frame and number of columns (= d)

coord_test       = vel_data.vel_depth_TT.world_coords_xyz{test_index}; % extract one (test) set of xyz coord of a given frame
vel_in_boundary  = inpolygon(coord_test(:,1),coord_test(:,2),boundary.boundary(:,1),boundary.boundary(:,2)); % find points (coord.) which are within pre-defined "boundary" --> true
num_pts_in_bound = sum(vel_in_boundary); % sum of "true" points, i.e. number of points within boundary

% preallocate (3D) arrays
% --> will be used later to applay sgolay filtering
vel_as_array   = zeros(num_frames,num_pts_in_bound,d);    % 3D array of velocity vectors, of size: frame_num x num_pts_in_bound x dimension (e.g. 36000x6000x3)
depth_as_array = zeros(num_frames,num_pts_in_bound,1);    % 2D array of flow depth, of size: frame_num x num_pts_in_bound x 1
coord_as_array = zeros(num_frames,num_pts_in_bound,d);    % 3D array of position (coord.), of size: frame_num x num_pts_in_bound x dimension (e.g. 36000x6000x3)
% is_peak = zeros(num_frames,num_pts_in_bound);           % unused???
% vel_mag = zeros(num_frames,num_pts_in_bound,1);         % unused???

% extract data from timetable
world_velocity_xyz = vel_data.vel_depth_TT.world_velocity_xyz;
world_coords_xyz   = vel_data.vel_depth_TT.world_coords_xyz;
world_flow_depth   = vel_data.vel_depth_TT.world_flow_depth;

% extract time from timetable
Time = vel_data.vel_depth_TT.Time;

% clear vel_data variable (which has been extracted above into constant arrays) to save RAM
clear vel_data

%% Pre-Processing: create giant 3D arrays by extracting data from Timetable

tic
disp('Creating 3D arrays...')

% for-loops for each velocity, coordinates (position) and flow detph
% --> take velocity/coord/flow depth matrix of frame i, and "stack" to giant 3D array of length frame_num (e.g. 36'000)

% N.B.: using parfor-loops instead of for-loops is NOT worthwile, because uses too much RAM and for-loop is already fast engouh!

% Velocity
for i = 1:num_frames

    vel_iter = world_velocity_xyz{i}; % extract velocity of frame i (of size num_pts_perFrame x 3)

    vel_as_array(i,:,:) = squeeze(vel_iter(vel_in_boundary,:)); % select "true" values which are in boundary (vel_in_boundary) from vel_iter, and save in array on "level" i (= current frame Nr.)

end

clear world_velocity_xyz


% Coordinates (position of vectors)
for i = 1:num_frames

    coord_iter = world_coords_xyz{i}; % extract coord. of frame i (of size num_pts_perFrame x 3)

    coord_as_array(i,:,:) = squeeze(coord_iter(vel_in_boundary,:)); % select "true" values which are in boundary (vel_in_boundary) (see above)

end

clear world_coords_xyz


% Flow Depth
for i = 1:num_frames

    depth_iter = world_flow_depth{i}; % extract flow depth of frame i

    depth_as_array(i,:) = squeeze(depth_iter(vel_in_boundary,:)); % select "true" values which are in boundary (vel_in_boundary) (see above)

end

clear world_flow_depth

toc

%% Processing: Smooth velocity array

disp('Smoothing velocities ...')

% Smoothing with Golay Filter:
vel_as_array_smooth_x = sgolayfilt(vel_as_array(:,:,1), polyorder, framelen);
vel_as_array_smooth_y = sgolayfilt(vel_as_array(:,:,2), polyorder, framelen);
vel_as_array_smooth_z = sgolayfilt(vel_as_array(:,:,3), polyorder, framelen);

% Smoothing golay-smoothed with movmean (to reduce noise):
vel_as_array_smooth = cat(3, vel_as_array_smooth_x, vel_as_array_smooth_y, vel_as_array_smooth_z); % combine 3 xyz-velocity vectors to single array
vel_as_array_smooth = movmean(vel_as_array_smooth, winlen, 'omitnan');

% Concatenate to TT
vel_depth_TT = timetable(Time,...
    coord_as_array,...
    vel_as_array,...
    vel_as_array_smooth,...
    depth_as_array,...
    'VariableNames',{'world_coords_xyz','world_velocity_xyz','world_velocity_xyz_smooth','world_depth'});

% Save velocities
save(fullfile(folder_paths.output_folder, sprintf('HS_vel_azi_alti_res%03.0f_SmoothGolay.mat', hs_params.spacing*1000)), 'vel_depth_TT', '-v7.3');
% save, with resolution included in filename

% Inform user:
fprintf('+++ Velocity Data Smoothed +++ \n File and Path: %s \n +++ +++ \n', fullfile(folder_paths.output_folder, sprintf('HS_vel_azi_alti_res%03.0f_SmoothGolay.mat', hs_params.spacing*1000)))

end % function "end"
