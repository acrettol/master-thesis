% 25.10.2024
% Jordan Aaron, modified Raffaele Spielmann and then by Arnaud Crettol
%
% -----------------------------------------------
% Script to find Sensor-Sensor-Transformation for LiDAR Data from 2 Sensors at CD27, Illgraben
% Run this Script LOCALLY (not on the cluster)
% ----------------------------------------------

clc
clear all
close all

addpath(genpath('Tools'));

%% Define Folder Paths and Frame Numbers --> CHANGE !!!

% Define Folder where Sensor-Sensor-Tform should be saved:
tform_save_folder = 'Transformations\ILL\2023\2023_06_02'; % folder to save transformation at end of script

% input folder for .ply files from OS1 and OS0 sensors
ply_directory_OS1 = 'C:\Users\arnau\Documents\ETH\Thesis_Backup\git_thesis\Matlab_Scripts\1_Generate_PLY\C_Merge_PLY\Owen';
ply_directory_OS0 = 'C:\Users\arnau\Documents\ETH\Thesis_Backup\git_thesis\Matlab_Scripts\1_Generate_PLY\C_Merge_PLY\Marcel';

% Frame Number to analyze / define ICP-transformation based on:
TestFrameNr   = 19; % Choose a frame befor flow arrival
% TestFrame_OS1 = 9;
% TestFrame_OS0 = 45; 

%% Define Transformation between Sensors and ROIs for ICP (OS1 IS Owen, 0S0 IS Marcel)

% Transformation for OS1 -> OS0 
Ry  = roty(0);
Rz  = rotz(0);
T   = [0,0,0];
tformCloud_OS1  = rigid3d(Ry*Rz,T); % -> no Transformation for OS1 (as OS1 is default coord.)

% Transformation for OS0 -> OS1
Ry  = roty(0);
Rz  = rotz(0);
T = [0,0,0];
OS0_OS1_rough_tForm = rigid3d(Ry*Rz,T);

% define ROIs where ICP should be applied on: (x/y/z range coordinates)
% applicable for all events at CD27 in 2023 (verfified by Arnaud Crettol)
OS0_ROI1 = [-5 3 -12 -6 -14 -8];    % ROI below check dam
OS0_ROI2 = [-3 2 -3.5 2 -7 -4];     % ROI at check dam
OS0_ROI3 = [-9 6 10 15 -6 0];       % ROI upstream

OS1_ROI1 = [-5 3 -12 -6 -14 -8];
OS1_ROI2 = [-3 2 -3.5 2 -7 -4];
OS1_ROI3 = [-9 6 10 15 -6 0];


% 2022_06_05 values at CD27
% OS0_ROI1 = [-6 4 -10 -7 -10 -8];    % ROI below check dam
% OS0_ROI2 = [-5 2 -4.5 2 -7 -4];     % ROI at check dam
% OS0_ROI3 = [-9 6 10 15 -6 0];       % ROI upstream
% 
% OS1_ROI1 = [-6 4 -10 -7 -10 -8];
% OS1_ROI2 = [-5 2 -4.5 2 -7 -4];
% OS1_ROI3 = [-9 6 10 15 -6 0];

%% Show results of (manual) alignment of two point clouds
% -> after Transformation T has been applied

% Load "TestFrameNr" Point Clouds from both Sensors:
LiDAR_Cloud_OS1 = pcread(fullfile(ply_directory_OS1,sprintf('%0.5d.ply', TestFrameNr))); % load OS1 Test point cloud
LiDAR_Cloud_OS0 = pcread(fullfile(ply_directory_OS0,sprintf('%0.5d.ply', TestFrameNr))); % load OS0
LiDAR_Cloud_OS0 = pctransform(LiDAR_Cloud_OS0,OS0_OS1_rough_tForm); % transformed (i.e. shifted) OS0 point cloud (to OS1)

% show results of "shift" (translation) of OS0 -> OS1
figure
pcshowpair(LiDAR_Cloud_OS0,LiDAR_Cloud_OS1)
title("os1 and os0 point clouds before ICP")


%% Crop point clouds to ROI

% Crop point clouds to ROIs:

% ROIs for OS1: find points in ROI and make new point cloud
indices1_OS1           = findPointsInROI(LiDAR_Cloud_OS1,OS1_ROI1);
indices2_OS1           = findPointsInROI(LiDAR_Cloud_OS1,OS1_ROI2);
indices3_OS1           = findPointsInROI(LiDAR_Cloud_OS1,OS1_ROI3);
indices_all_OS1        = [indices1_OS1; indices2_OS1; indices3_OS1];
LiDAR_Cloud_OS1_Select = select(LiDAR_Cloud_OS1,indices_all_OS1); % crop OS1

% ROI for OS0
indices1_OS0           = findPointsInROI(LiDAR_Cloud_OS0,OS0_ROI1);
indices2_OS0           = findPointsInROI(LiDAR_Cloud_OS0,OS0_ROI2);
indices3_OS0           = findPointsInROI(LiDAR_Cloud_OS0,OS0_ROI3);
indices_all_OS0        = [indices1_OS0; indices2_OS0; indices3_OS0];
LiDAR_Cloud_OS0_Select = select(LiDAR_Cloud_OS0,indices_all_OS0); % crop OS0

% show croped point clouds in ROIs
figure
pcshowpair(LiDAR_Cloud_OS0_Select,LiDAR_Cloud_OS1_Select)
title("os1 and os0 point clouds: selected ROIs (used for ICP)")

%% Apply ICP registration to cropped ptClouds

% apply ICP to ROIs of point clouds

% apply pcregistericp(movingPtCloud, fixedPtCloud, ...) and output Tform
[OS0_OS1_ICP_tForm,movingReg] = pcregistericp(LiDAR_Cloud_OS0_Select, LiDAR_Cloud_OS1_Select,...
    "Metric","pointToPlane","Tolerance",[0.000001 0.00001],"MaxIterations",5000);

% transform OS0 -> OS1
LiDAR_Cloud_OS0_T = pctransform(LiDAR_Cloud_OS0,OS0_OS1_ICP_tForm);

% show ICP-registered point clouds
figure
pcshowpair(LiDAR_Cloud_OS0_T,LiDAR_Cloud_OS1,MarkerSize=10)
ylim([-35,50])
title("os1 and os0 point clouds AFTER ICP-alignment -> Check Quality!")
    xlim([-25 25])
    ylim([-50 30])
    zlim([-15 5])

%% check with cross section
ptCloudRasterSpacing = 0.1; %point cloud rasterization spacing
sectionSpacing       = 0.1; %spacing used for sampling points on the cross section
x = [-20,20]; %x limits of hillshade
y = [-30,30]; %y limits of hillshade
legendString={'OS0', 'OS1'};
make_cross_section(LiDAR_Cloud_OS1,LiDAR_Cloud_OS0_T,ptCloudRasterSpacing,...
    sectionSpacing,x,y,legendString)

%% Save Transformations TFORM between OS1->OS0

% Save in "Transformation" Folder for given event (on Git):
transformation_filename = 'CD27_MarcelToOwen_SensorSensor_tform_mod2.mat';
save(fullfile(tform_save_folder, transformation_filename),'OS0_OS1_ICP_tForm', 'OS0_OS1_rough_tForm') % save SensorSensor-tForms
% --> "rough" transformation (shift by some meters) and then detailed transformation, according to ICP

fprintf('DONE! %s saved under: %s \n',transformation_filename, tform_save_folder)
