function B_generate_intermediate_PLY(configuration_file)
%Function to generate PLY files, from .mat file based on pre-defined tForm-Sensor (with or without IMU-Vertical-Correction)
%On the ETH Euler Cluster

addpath(genpath('Tools'));

load(configuration_file);  % load config file (.mat / .json)
load(tform_filename);  % load fForm Sensor file, as defined in "Determine_Sensor_Transformation.m"

%% output PLY files

total_number = end_frame - start_frame + 1; % total number of frames
increment = ceil(total_number/101);
sequence = [start_frame:increment:end_frame];
sequence(end) = end_frame;

iter_time = NaN;

local_job = parcluster('local');
pool = parpool(local_job, num_cores);
disp('Initialisation done')

% Weird workaround to avoid parfor "not found" error:
folder_mat = mat_file_folder;
folder_output = ply_output_folder;
tForm_parfor = tForm;

for j = 1:100

    cur_sequence = [sequence(j) sequence(j+1)];
    time_to_finish = seconds(iter_time*(100-j+1));
    time_at_finish = datetime('now')+time_to_finish;
    status_message = sprintf('the calculation is: %d%% finished, expected time to finish is: %s',...
        j-1,...
        time_at_finish);
    disp(status_message)
    tic

   parfor i = cur_sequence(1):cur_sequence(2)
             %disp(mat_file_folder)
        ptCloudFileName = fullfile(folder_mat,sprintf('frame_destaggered_%d.mat',i));
        % tic
        LiDAR_Cloud = mat_2_pt_cloud(ptCloudFileName,tForm_parfor);
        pcwrite(LiDAR_Cloud,fullfile(folder_output,sprintf('%0.5d.ply',i)));
   end

    iter_time = toc;

end
pool.delete();

end