function C_CD27_merge_generate_PLY(OS0OS1_Tform_path, ply_folder_OS1, ply_folder_OS0, output_folder_merged, StartFrame, EndFrame, num_cores)
% 25.10.2024
% Jordan Aaron, modified Raffaele Spielmann and then by Arnaud Crettol
% (Technically this is the same function for CD27 and CD29)
%
% Function to processes .ply files in order to merge them from two sensors (OS1 and OS0)
% --> run LOCAL script first, to determine Sensor-Sensor-tForm !!!


%% Define Folder Paths and Frame Numbers

load(OS0OS1_Tform_path) % load SensorSensor-Transformation, Variable Name: "OS0_OS1_tForm"

% 3D-Box (voxel) gridStep Size for "pcmerge" (size of 3D-box to merge points), in m:
VoxelGridStep = 0.01; % e.g. 1 cm


%% transform all OS0 frames, and merge with OS1.  Output results to separate

total_number  = EndFrame - StartFrame + 1; % total number of frames
increment     = ceil(total_number/101);
sequence      = [StartFrame:increment:EndFrame];
sequence(end) = EndFrame;

iter_time = NaN;

local_job = parcluster('local');
pool      = parpool(local_job, num_cores);

% weird workaround:
tForm_OS0_OS1_rough = OS0_OS1_rough_tForm; % first, rough transformation (manual shift by few m)
tForm_OS0_OS1_ICP   = OS0_OS1_ICP_tForm;   % tForm from ICP (between shifted ptClouds)

for j = 1:100

    cur_sequence   = [sequence(j) sequence(j+1)];
    time_to_finish = seconds(iter_time*(100-j+1));
    time_at_finish = datetime('now')+time_to_finish;
    status_message = sprintf('the calculation is: %d%% finished, expected time to finish is: %s',...
        j-1,...
        time_at_finish);
    disp(status_message)
    tic

    parfor i = cur_sequence(1):cur_sequence(2)

        % load (rotated, to channel coord.) .ply files:
        LiDAR_Cloud_OS1 = pcread(fullfile(ply_folder_OS1,sprintf('%0.5d.ply',i))); % load OS1 .ply
        LiDAR_Cloud_OS0 = pcread(fullfile(ply_folder_OS0,sprintf('%0.5d.ply',i))); % load OS0 .ply

        % Transform:
        LiDAR_Cloud_OS0_S = pctransform(LiDAR_Cloud_OS0, tForm_OS0_OS1_rough); % output: shifted (by few m) OS0 ptCloud (towards OS1)
        LiDAR_Cloud_OS0_T = pctransform(LiDAR_Cloud_OS0_S, tForm_OS0_OS1_ICP); % output: OS0 -> OS1 transformed ptCloud for OS0 
        
        % Remove overlapping points from OS0: NEW FUNCTION (Arnaud Crettol, found in 'Tools' folder)
        LiDAR_Cloud_OS0_T_mod = OS0_overlap_removal(LiDAR_Cloud_OS0_T, LiDAR_Cloud_OS1)
        
        % Merge:
        LiDAR_Cloud_OS1_OS0_merged = pcmerge(LiDAR_Cloud_OS1,LiDAR_Cloud_OS0_T_mod, VoxelGridStep); % output merged ptCloud (with defined GridStep)
        
        % Save / Output:
        pcwrite(LiDAR_Cloud_OS1_OS0_merged, fullfile(output_folder_merged,sprintf('%0.5d.ply',i))); % save MERGED cloud
        % pcwrite(LiDAR_Cloud_OS0_T, fullfile(output_folder_OS0_T,sprintf('%0.5d.ply',i))); % save OS0 cloud & --> OS0 in OS1-coord. NOT saved at the moment
    end

    iter_time = toc;
end

pool.delete();

end
