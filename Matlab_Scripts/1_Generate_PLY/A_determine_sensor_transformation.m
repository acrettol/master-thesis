% 25.10.2024
% Jordan Aaron, modified Raffaele Spielmann and then by Arnaud Crettol

% ------------------------------------------------------------------
% Script to Determine the transformation from sensor to channel coord., incl. IMU vertical correction
% ------------------------------------------------------------------
% A script to determine the transformation between sensor and channel coordinate systems.
% The script uses the IMU data to estimate the vertical axis, and then the user must determine the other transformation parameters.
% The desired coordinate system is that:
% Y: parallel to the channel and positive upstream
% Z: Vertical and positive upwards
% X: Cross channel and positive to orographic left

clc
clear all
addpath(genpath('Tools'));

%% Define Sensor and Input Data
% Sensor Name and Path
sensor_name       = 'Gazo';    %'Gazo', 'Pip', 'Bloom', 'Marcel', 'Owen'
year              = '2023';
date              = '2023_07_12';
mat_file_folder   = 'C:\Users\arnau\Documents\ETH\Thesis_Backup\git_thesis\Matlab_Scripts\1_Generate_PLY\A_MAT_Transfo\2023_07_12'; %select mat file folder
tform_save_folder = fullfile(sprintf('Transformations/ILL/%s/%s',year,date)); %select folder to save transformation at end of script

FrameIndex        = 27; %frame number (index) of point cloud frame to load

% 2023 events > gazo=27, bloom=21, owen=15, marcel=55

%% Select if IMU-Vertical-Correction (to correct for not-perfectly-vertical sensor) is wanted or not:
IMU_rotation_select = "No"; % "Yes" or "No"
                            % --> usually:  % Yes for CD27 (Marcel & Owen)
                                            % No for Gazoduc and CD29 (Gazo & Bloom & Pip)

% Definition of vertical axis:
sensor_vertical_vect = [1, 0, 0]; % Owen = [1,0,0], Marcel = [1, 0, 0]

%change parameters below to find appropriate transformation from sensor coord. --> channel coord. (All values are verified and correct)
Rx = rotx(0);     % Gazo=0    Owen/Marcel(23)=0    Owen/Marcel(22)=0       Bloom(23)=0     Bloom/Pip(22)=0,    Pip(23)=0
Ry = roty(90);    % Gazo=90   Owen/Marcel(23)=90   Owen/Marcel(22)=-90     Bloom(23)=90    Bloom/Pip(22)=-90   Pip(23)=180
Rz = rotz(180);   % Gazo=180  Owen/Marcel(23)=180  Owen/Marcel(22)=0       Bloom(23)=180   Bloom/Pip(22)=0     Pip(23)=0
T = [0,0,0];

% get tranformation from sensor coord. to channel coord., with vertical axis NOT being perfectly aligned
tformSensor = rigid3d(Ry*Rx*Rz,T);

%% IMU vertical correction OR without correction

if IMU_rotation_select == "Yes" % if IMU-vertical correction wanted

    % get information from IMU, to have vertical axis perfectly aligned:
    rot_imu = imu_rotation_no_wobble(fullfile(mat_file_folder,sprintf('%s_IMU.txt',sensor_name)),sensor_vertical_vect);

    % Combine tformSensor and IMU to get final tForm
    rot_sensor = tformSensor.Rotation; % sensor tform
    rot_sensor_imu = rot_sensor*rot_imu; % combination
                                         % NOTE: matrix mulitplication is not commutative. Here, it means that rot_imu is FIRST applied and then rot_sensor is SECOND applied (to intermed. result).

    tForm = rigid3d(rot_sensor_imu,tformSensor.Translation); % final tForm, including IMU

elseif IMU_rotation_select == "No" % if NOT wanted
    tForm = tformSensor; % final tForm, without IMU

end

%% output PLY file and visualize

figure
      
    ptCloudFileName = fullfile(mat_file_folder,sprintf('frame_destaggered_%d.mat',FrameIndex)); % load .mat file (point cloud)

    LiDAR_Cloud = mat_2_pt_cloud(ptCloudFileName,tForm); % transform, according to tFrom_IMU
    
    pcshow(LiDAR_Cloud,'AxesVisibility','on') % visualize
    xlabel('X (channel-perp.)')
    ylabel('Y (channel-parallel)')
    zlabel('Z (vertical)')
%     xlim([-25 25]) % marcel zoom
%     ylim([-50 30]) % marcel zoom
%     zlim([-15 5])  % marcel zoom

    %title('CHECK axis and orientation (should be: z = vertical up, y = channel parallel up, x = to orographic left)')
    title('Raw data from LiDAR sensor at Owen station')

%% save results

transformation_filename = sprintf('%s_sensor_tform.mat',sensor_name);
save(fullfile(tform_save_folder, transformation_filename),'tForm')