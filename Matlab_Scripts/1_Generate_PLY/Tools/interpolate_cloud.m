function [F] = interpolate_cloud(ptCloud)
%%A function to create a scattered interpolant of an input point cloud, and
%%generate interpolated values based on an input spacing, and then
%%generate a hillshade
xyz = double(ptCloud.Location);
xyz(isnan(xyz(:,1)),:) = 0;
x = xyz(:,1);
y=xyz(:,2);
z=xyz(:,3);
F = scatteredInterpolant(x,y,z);
F.Method = 'linear';
F.ExtrapolationMethod = 'none';

end