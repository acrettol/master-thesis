function make_cross_section(ptCloud1, ptCloud2,ptCloudRasterSpacing,sectionSpacing,x,y,legendString)

%%Display hillshade and get section line
azimuth = 90;
altitude = 45;
figure
[H,~,~, ~,~] = createScanHillshade_Azimuth_Elevation(ptCloud1,...
    ptCloudRasterSpacing,x,y,azimuth,altitude);
I = mat2gray(H);
R = imref2d(size(I),x,y);
imshow(I,R)
% hold on
% plot(ptCloud1.Location(:,1),ptCloud1.Location(:,2),'.','MarkerSize',0.1);
roi = drawpolyline;
sectionLine = roi.Position;
%%Use digitized section line to make cross section

%determine if the cross section is primarily in the x or y direction
if max(sectionLine(:,1))-min(sectionLine(:,1))>max(sectionLine(:,2))-min(sectionLine(:,2)) %primarily an x-axis section
    xEval = min(sectionLine(:,1)):sectionSpacing:max(sectionLine(:,1));
    Vq = interp1(sectionLine(:,1),sectionLine(:,2),xEval);
    xSection = xEval;
    ySection = Vq;
else %primarily a y-axis section line
    xEval = min(sectionLine(:,2)):sectionSpacing:max(sectionLine(:,2));
    Vq = interp1(sectionLine(:,2),sectionLine(:,1),xEval);
    xSection = Vq;
    ySection =xEval;
end

%extract cross section, F is the interpolation created for the hillshade
ptCloud1_interp = interpolate_cloud(ptCloud1);
ptCloud1_Pts  = ptCloud1_interp(xSection,ySection);

ptCloud2_interp = interpolate_cloud(ptCloud2);
ptCloud2_Pts  = ptCloud2_interp(xSection,ySection);

figure
plot(horizDistance(xSection,ySection),ptCloud1_Pts,'-r','LineWidth',2);
hold on
plot(horizDistance(xSection,ySection),ptCloud2_Pts,'-b','LineWidth',2);

axis equal
legend(legendString);
xlabel('distance along section (m)');
ylabel('elevation (m)')
movegui(gca, [0 0])
hold off

end
