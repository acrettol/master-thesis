function r = imu_rotation_no_wobble(imu_txt_file_name, sensor_vertical_vect)
% IMU_Data = readtable("\\82.130.98.56\LiDAR_Uncompressed_Data\July_04_2022\SP27\Owen\Owen_IMU.txt");
IMU_Data = readtable(imu_txt_file_name);


linearAcceleration = table(IMU_Data.Var26,IMU_Data.Var27,IMU_Data.Var28); %Variable numbers might change?
linearAcceleration.Properties.VariableNames = ["x_accel","y_accel","z_accel"];
norm_accel_vect = vecnorm([linearAcceleration.x_accel,linearAcceleration.y_accel,linearAcceleration.z_accel],2,2); % magnitude / length of acceleration vector (gravity)

norm_gravity_norm_vec = [linearAcceleration.x_accel,linearAcceleration.y_accel,linearAcceleration.z_accel]./norm_accel_vect;

%take added rotation as the mean of the three acceleration directions
norm_gravity_vec_mean = mean(norm_gravity_norm_vec);

% r = get_rot_vec(norm_vec_mean(1,:),[1 0 0]);

%Raffaele
%r = vrrotvec(norm_gravity_vec_mean,sensor_vertical_vect); % find rotation, so that vect. A gets rotated to vect. B --> ORDER of these two vectors might have to be changed for unknown reasons !

%Arnaud
r = vrrotvec(sensor_vertical_vect,norm_gravity_vec_mean); % find rotation, so that vect. A ("vertical" axis of sensor) gets rotated to vect. B ("true" vertical, according to gravity, from IMU)
r(4)=-1.*r(4); %the last element defines the angle of rotation => the point cloud should be turn in the other direction so that the check dam is parallel to the x axis (flat), and not the sensor (which is inclined in real life)

r = vrrotvec2mat(r); % change format of "r" to a matrix

% Output mean linear acceleration to inform user:
lin_accel_mean = mean([linearAcceleration.x_accel,linearAcceleration.y_accel,linearAcceleration.z_accel]);
fprintf("Mean acceleration in x,y,z (sensor coord. system) is: \n %.2f, %.2f, %.2f \n", lin_accel_mean)

% for i = 1:length(linearAcceleration.x_accel)
%     rotated_accel(i,:) = [r*[linearAcceleration.x_accel(i),linearAcceleration.y_accel(i),linearAcceleration.z_accel(i)]']';
% end
end

function [R] = get_rot_vec(p0,p1)
%GET_ROT_VEC Summary of this function goes here
%   Detailed explanation goes here
% calculate cross and dot products
C = cross(p0, p1) ; 
D = dot(p0, p1) ;
NP0 = norm(p0) ; % used for scaling
if ~all(C==0) % check for colinearity    
    Z = [0 -C(3) C(2); C(3) 0 -C(1); -C(2) C(1) 0] ; 
    R = (eye(3) + Z + Z^2 * (1-D)/(norm(C)^2)) / NP0^2 ; % rotation matrix
else
    R = sign(D) * (norm(p1) / NP0) ; % orientation and scaling
end
end


