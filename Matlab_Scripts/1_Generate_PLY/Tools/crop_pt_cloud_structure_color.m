function ptCloud_Cropped = crop_pt_cloud_structure_color(ptCloud,xLim_Crop,yLim_Crop)

indices_crop = ptCloud.Location(:,:,1)> xLim_Crop(1) & ...
    ptCloud.Location(:,:,1)< xLim_Crop(2) & ...
    ptCloud.Location(:,:,2)> yLim_Crop(1) & ...
    ptCloud.Location(:,:,2)< yLim_Crop(2);
%crop point cloud while preserving structure
x_Crop = ptCloud.Location(:,:,1);
x_Crop(~indices_crop) = 0;

x_Crop = x_Crop(:, any(x_Crop, 1));  % Keep only columns with any non-zero value
x_Crop = x_Crop(any(x_Crop, 2), :);  % Keep only rows with any non-zero value

y_Crop = ptCloud.Location(:,:,2);
y_Crop(~indices_crop) = 0;

y_Crop = y_Crop(:, any(y_Crop, 1));  % Keep only columns with any non-zero value
y_Crop = y_Crop(any(y_Crop, 2), :);  % Keep only rows with any non-zero value

z_Crop = ptCloud.Location(:,:,3);
z_Crop(~indices_crop) = 0;

z_Crop = z_Crop(:, any(z_Crop, 1));  % Keep only columns with any non-zero value
z_Crop = z_Crop(any(z_Crop, 2), :);  % Keep only rows with any non-zero value

col_Crop_R = ptCloud.Color(:,:,1);
col_Crop_R(~indices_crop) = 0;

col_Crop_R = col_Crop_R(:, any(col_Crop_R, 1));  % Keep only columns with any non-zero value
col_Crop_R = col_Crop_R(any(col_Crop_R, 2), :);  % Keep only rows with any non-zero value

col_Crop_G = ptCloud.Color(:,:,2);
col_Crop_G(~indices_crop) = 0;

col_Crop_G = col_Crop_G(:, any(col_Crop_G, 1));  % Keep only columns with any non-zero value
col_Crop_G = col_Crop_G(any(col_Crop_G, 2), :);  % Keep only rows with any non-zero value3.

col_Crop_B = ptCloud.Color(:,:,3);
col_Crop_B(~indices_crop) = 0;

col_Crop_B = col_Crop_B(:, any(col_Crop_B, 1));  % Keep only columns with any non-zero value
col_Crop_B = col_Crop_B(any(col_Crop_B, 2), :);  % Keep only rows with any non-zero value

ptCloud_Cropped = pointCloud(cat(3,x_Crop,y_Crop,z_Crop));
ptCloud_Cropped.Color = cat(3,col_Crop_R,col_Crop_G,col_Crop_B);

end