function hD = horizDistance(X,Y)
%A function to determine the distance along a cross section based on x and
%y point coordinates
X = X-X(1);
Y=Y-Y(1);

hD = sqrt(X.^2+Y.^2);

end