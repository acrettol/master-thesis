function LiDAR_Cloud_OS0_T_mod = OS0_overlap_removal(OS0_T, OS1)

% Extract both point clouds boundaries as polyshapes
% By Arnaud Crettol (02.07.2024) => in order to remove OS0 points where it 
% is overlapping the OS1 point cloud (no artifacts)
points1 = OS0_T.Location;
points2 = OS1.Location;

% Project to the XY plane (ignoring z-coordinate)
xyPoints1 = double(points1(:, 1:2));
xyPoints2 = double(points2(:, 1:2));

% Use the boundary function to find the boundary points
k1 = boundary(xyPoints1,1);
k2 = boundary(xyPoints2,1);

% Extract the boundary points
boundaryPoints1 = xyPoints1(k1, :);
boundaryPoints2 = xyPoints2(k2, :);

% Ensure the boundary forms a closed polygon
boundaryPoints1 = [boundaryPoints1; boundaryPoints1(1, :)];
boundaryPoints2 = [boundaryPoints2; boundaryPoints2(1, :)];

% Create polyshape objects for each boundary
poly1 = polyshape(boundaryPoints1(:, 1), boundaryPoints1(:, 2));
poly2 = polyshape(boundaryPoints2(:, 1), boundaryPoints2(:, 2));

%%Remove points from OS0 from inside the calculated intersection polygon
% Compute the intersection of the two polygons
intersectionPoly = intersect(poly1, poly2);

% Extract the intersection boundary points
intersectionPoints = intersectionPoly.Vertices;

% Create a 3D polygon for the intersection area
intersectionPoints3D = [intersectionPoints, ones(size(intersectionPoints, 1), 1) * mean(points1(:, 3))];  % Assuming z-coordinate is averaged

% Check which points from OS0 are inside the intersection polygon
inIntersection = inpolygon(points1(:, 1), points1(:, 2), intersectionPoints3D(:, 1), intersectionPoints3D(:, 2));

% Remove points inside the intersection polygon from PC1
points1_filtered = points1(~inIntersection, :);

% Create a new point cloud object for visualization
LiDAR_Cloud_OS0_T_mod = pointCloud(points1_filtered);
