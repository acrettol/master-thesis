% 25.10.2024
% Jordan Aaron, modified Raffaele Spielmann and then by Arnaud Crettol

% Script to generate .json and .mat files which can then be used to
% generate .ply files from .mat files on the Cluster
clc
clear all
close all

intermediate_ply_params = struct();

%% Define Names and Path (change for your own scratch directory)

catchment   = 'ILL';
loc_name    = 'Gazoduc';    % 'Gazoduc', 'CD29', 'CD27'
sensor_name = 'Gazo';       % 'Gazo', 'Bloom', 'Pip', 'Owen', 'Merged'
year        = '2023';
event_date  = '2023_07_12';

mat_file_folder = sprintf('/cluster/scratch/acrettol/Data/%s/%s/MAT_files/%s_mat',event_date,loc_name,sensor_name);
% ply_output_folder = sprintf('/cluster/scratch/acrettol/Data/%s/%s/PLY_files',event_date,loc_name); % only for Gazoduc 07_04_2022
ply_output_folder = sprintf('/cluster/scratch/acrettol/Data/%s/%s/PLY_files/%s_ply',event_date,loc_name,sensor_name); % use this line

tForm_transformation_folder = sprintf('Transformations/%s/%s/%s', catchment, year, event_date); % no need to change based on the .ZIP folder structure
config_file_save_folder = sprintf('Configuration_Files/%s/%s/%s', catchment, year, event_date);

% Create folders if they do not exist:
if ~exist(tForm_transformation_folder, 'dir')
    mkdir(tForm_transformation_folder);
end

if ~exist(config_file_save_folder, 'dir')
    mkdir(config_file_save_folder);
end

% Save filenames and paths:
tform_filename  = sprintf('%s/%s_sensor_tform.mat',tForm_transformation_folder,sensor_name); % do NOT use "fullfile", as this causes "backslash" (rather than forwardslash) on Windows
config_filename = fullfile(config_file_save_folder,sprintf('%s_%s_config',sensor_name, event_date));

%% Define Frames and Cores
% Number of First Frame and Total Number of Frames --> CHANGE !!!
start_frame = 1;
end_frame   = 19973;

% Numbers of Cores:
num_cores   = 82; % up to 128

%% Save in Struct and as json / mat

intermediate_ply_params.sensor_name       = sensor_name;
intermediate_ply_params.mat_file_folder   = mat_file_folder;
intermediate_ply_params.output_folder     = ply_output_folder;
intermediate_ply_params.end_frame         = end_frame;
intermediate_ply_params.start_frame       = start_frame;
intermediate_ply_params.num_cores         = num_cores;
intermediate_ply_params.sensor_tform_name = tform_filename;

output_json(config_filename, intermediate_ply_params); % save .json
save(sprintf('%s.mat',config_filename)); % save .mat

function output_json(output_filename,intermediate_ply_params)

    intermediate_ply_params_encode = jsonencode(intermediate_ply_params,PrettyPrint=true);
    
    filename = sprintf('%s.json',output_filename);  % better to use fullfile(path,name) 
    fid = fopen(filename,'w');    % open file for writing (overwrite if necessary)
    fprintf(fid,'%s','intermediate_ply_params');          % Write the char array, interpret newline as new line
    fprintf(fid,'%s',intermediate_ply_params_encode);          % Write the char array, interpret newline as new line
    fclose(fid);                  % Close the file (important)

end