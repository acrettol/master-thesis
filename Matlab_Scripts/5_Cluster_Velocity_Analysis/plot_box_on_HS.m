% ETH Zürich, 25.10.2024
% Arnaud Crettol

% ------------------------
% Script to visualize the area used to extract velocities on the hillshades
% of a given event at a given station.
% ------------------------

clear, clc, close all

addpath(genpath('Tools'));
%% load in any corresponding point cloud (.ply File on Tiliva) (for visualization purposes)
[ptcFilename, ptcPath] = uigetfile('*.ply', 'Select an (early) Point Cloud of this Event (.ply)');
ptCloudPlot = pcread(fullfile(ptcPath, ptcFilename)); % load ptCloud
sensor_name = ptcPath(71:74);
date_n      = ptcPath(60:69);
date_n(5)   = '.';
date_n(8)   = '.';
date_n = [date_n(9) date_n(10) date_n(8) date_n(6) date_n(7) date_n(5) date_n(1) date_n(2) date_n(3) date_n(4)];

%% Hillshade Parameters
azimuth = 90;
altitude = 45;
ptCloudRasterSpacing = 0.05;

xLim = [-15 15]; %[-12 12] Gazo
yLim = [-50 50]; %[-50 50] Gazo
numBoxes = 1; %number of boxes to view

%% Select and Extract Velocities

% Create a Hillshade from the selected point cloud for plotting as base
[H,xRange,yRange,interpolatedPtCloud,F] = createScanHillshade_Azimuth_Elevation(ptCloudPlot,ptCloudRasterSpacing,xLim,yLim,azimuth,altitude); %generate hillshade for base
frame = mat2gray(H);
R = imref2d(size(frame),xLim,yLim);

% Specify box coordinates, ideally 15m from sensor (=/= from check dam).
% Keep it the same across all events and stations
figure(Position=[1200 100 200 600])
imshow(frame,R)
xBox = input('Please enter xBox coordinates (e.g., [1 2]): ');
yBox = input('Please enter yBox coordinates (e.g., [1 2]): ');
hold on;

% Specify the x and y coordinates of the square's corners Bottom-left > Bottom-righ > Top-right > Top-left
x = [xBox(1) xBox(2) xBox(2) xBox(1)];
y = [yBox(2) yBox(2) yBox(1) yBox(1)];

% Draw the square using the 'fill' function
fill(x, y, 'r', 'FaceAlpha', 0.6);  % 'r' specifies red color, 'FaceAlpha' adds transparency
title([date_n,' - ',sensor_name],'FontSize',15)