

function legendString = generate_legend_string(numBoxes,velocity_dataset_names,yBox)

dataset_name = repmat(velocity_dataset_names,numBoxes,1);
dataset_name= dataset_name(:);

boxName = cellstr(num2str(yBox));
boxName = repmat(boxName,length(velocity_dataset_names),1);
boxName = boxName(:);

legendString = join([dataset_name,boxName]);
end