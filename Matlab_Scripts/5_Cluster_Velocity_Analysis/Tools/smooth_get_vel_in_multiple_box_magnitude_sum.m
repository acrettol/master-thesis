
function velocity_in_box = smooth_get_vel_in_multiple_box_magnitude_sum(velocity_TT,xBox,yBox,x_vel_min, x_vel_max, y_vel_min, y_vel_max, magnitude)

[numBoxes,dummyvar] = size(xBox);

for i = 1:numBoxes
    [velocity_in_box(:,i),location] = smooth_get_mean_vel_box_magnitude_sum(velocity_TT.world_coords_xyz,...
        velocity_TT.world_velocity_xyz_smooth,...
                                                                 xBox(i,:), yBox(i,:),...
                                                                 x_vel_min, x_vel_max, y_vel_min, y_vel_max,magnitude);
end

end
