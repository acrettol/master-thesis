function [xBox,yBox] = smooth_get_box_coord_gui_include_R(frame,velocity_TT,numBoxes,close,index,R)
xBox = zeros(numBoxes,2);
yBox = zeros(numBoxes,2);
%define box(es)
% index = 1000;
boxFig = figure;
imshow(frame,R);
hold on
pos = squeeze(velocity_TT.world_coords_xyz(index,:,:));
vel = squeeze(velocity_TT.world_velocity_xyz(index,:,:));
quiver(pos(:,1),pos(:,2),...
    vel(:,1),vel(:,2));
% xlim([min(velocity_TT.world_coords_xyz{index}(:,1)), max(velocity_TT.world_coords_xyz{index}(:,1))]);
% ylim([min(velocity_TT.world_coords_xyz{index}(:,2)), min(max(velocity_TT.world_coords_xyz{index}(:,2),max(yLim)))]);
for i = 1:numBoxes
    roi = drawrectangle;
    xBox(i,:) = [floor(roi.Position(1)) ceil(roi.Position(1)+roi.Position(3))];
    yBox(i,:)= [floor(roi.Position(2)) ceil(roi.Position(2)+roi.Position(4))];
end

if close == 1
    close(boxFig)
end

end