%a function to extract filtered velocity vectors in a given box and return
%mean of the velocities in the box
function [meanVel,location] = smooth_get_mean_vel_box_magnitude_sum(position,velocity,...
    xBox, yBox,x_vel_min, x_vel_max, y_vel_min, y_vel_max,magnitude)

for i = 1:length(position)
    %get indices in box

    if(~isempty(squeeze(position(i,:,:))))
        [indices] = getVelInBox(xBox, yBox, squeeze(position(i,:,:)));

        %extract position and velocity
        location = squeeze(position(i,:,:));
        vel = squeeze(velocity(i,:,:));

        location=location(indices,:);
        vel=vel(indices,:);

        %filter vectors based on maxima
        filter_indices = vel(:,1) > x_vel_min &...
            vel(:,1) < x_vel_max &...
            vel(:,2) > y_vel_min & ...
            vel(:,2) < y_vel_max & ...
            vecnorm(vel,2,2) > magnitude;
        vel = vel(filter_indices,:);

    
    mean_vel = mean(vel);
    %compute magnitudes and mean velocity in box
    vel_mag = vecnorm(mean_vel,2,2);
    meanVel(i)=vel_mag;%mean(vel_mag);
% meanVel(i)=max(vel_mag);
    else
        meanVel(i)=0;
    end
end

end

function [indices] = getVelInBox(xBox, yBox, VectOrig)
%assumes that limits are [xMin xMax] [yMin yMax], and that vector orig
%is a 3 x n vector with xyz coords in each row
indices = VectOrig(:,1)>=xBox(1) & VectOrig(:,1)<=xBox(2) & ...
    VectOrig(:,2)>=yBox(1) & VectOrig(:,2)<=yBox(2);

end


