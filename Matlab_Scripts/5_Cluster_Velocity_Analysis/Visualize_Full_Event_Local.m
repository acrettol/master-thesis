% ETH Zürich, 25.10.2024
% Arnaud Crettol

% ------------------------
% Script to visualize velocities extracted from an user selected box in the
% Toolbox PIVlab. The mean velocity magnitude is stored in a .xls file.
% Step 1: rename the column n°2 of the .xls file to "mean_velo_mag"
% Step 2: add one column in the first position, called "FrameNr"
% Step 3: Fill in this column automatically with all the Frames analyzed
% Step 4: Save the Excel Table as .xlsx file
% Then it is ready to be processed using this Matlab script (option, add
% the manual labels "MeanVelocities_perObjClass_TT.mat" of the same event)
% ------------------------

clc
clear all
close all

disp_GTL      = 1; % Display GTL tracking on the graph, 1=yes 0=no
time_interval = 5; %Interval in minutes between the ticks in the final plot

%% Plot the data of the mean area for the debris flow event
file_path   = 'Doc_Excel.xlsx';     % To plot the full dataset
mean_area   = readtable(file_path);
x_data      = mean_area.FrameNr;        % Frame number (x)
y_data      = mean_area.mean_velo_mag;  % Mean area velocity magnitude (y)

% Moving mean and Savitzky-Golay filtering
order       = 3;                   % polynomial order for golay
framelen    = 45;                  % frame length for golay
sgf         = sgolayfilt(y_data,order,framelen);

% Scatterplot, for a better visibility
numBoxes = 1; %number of boxes to view
velocity_dataset_names = {'HS Velocity'};
figure
h1 = scatter(x_data,y_data,".b");
xlim([0,16800])
ylim([0,12])
ylabel('Mean area velocity magnitude [m/s]','FontSize',15)
grid on
box on
hold on

%% Extract GTL Classes and plot them on the same graph
load('MeanVelocities_perObjClass_TT.mat');

time_GTL    = MeanVel_perClass_TT.Time; %extract the time column
seconds_GTL = seconds(time_GTL); %convert durations to seconds
frames_GTL  = round(10.*seconds_GTL); %convert seconds to frames, round up

matrix_GTL  = [MeanVel_perClass_TT.front_MeanVel MeanVel_perClass_TT.driftW_MeanVel MeanVel_perClass_TT.boulder_MeanVel MeanVel_perClass_TT.rollB_MeanVel]; % extract the 4 classes values
names_GTL   = MeanVel_perClass_TT.Properties.VariableNames';  % extract the 4 classes names
colors_GTL  = [0 0 0;0 1 0;0.9290 0.6940 0.1250;1 0.5 0.8];   % extract the 4 classes colors
symbols_GTL = {'^', 'd', 's', 'o'};         % classes symbols
classes_GTL = zeros(size(matrix_GTL,1),1);  % initialise empty vector to store classes values
types_GTL   = rand(size(matrix_GTL,1),3);   % initialise empty vector to store classes colors

% Extract each not NaN class values in matrix_GTL, along with their classes names
for i=1:size(matrix_GTL,1)
    for j=1:size(matrix_GTL,2)
        if matrix_GTL(i,j)>0
            classes_GTL(i) = matrix_GTL(i,j); % Class values
            types_GTL(i,:) = colors_GTL(j,:); % Class colors (names)
        end 
    end
end

% Plot each class separately with different symbols
for j = 1:size(matrix_GTL, 2)
    scatter(frames_GTL, matrix_GTL(:, j), 100, colors_GTL(j, :), symbols_GTL{j}, 'filled');
    hold on
end

%% Create and use invisible points for legend entries, then combine legend
legend_entries = gobjects(4, 1);  % Preallocate array for legend entries
for j = 1:size(matrix_GTL, 2)
    legend_entries(j) = scatter(NaN, NaN, 2000, colors_GTL(j, :), symbols_GTL{j}, 'filled');
    hold on
end

if disp_GTL == 1
    ColorOrder = turbo(length(velocity_dataset_names)*numBoxes);
    set(gcf, 'Position', get(0,'Screensize'));
    h2 = plot(x_data,sgf,'r','LineWidth',1);
    title('Mean surface velocity of the 04.07.2022 debris flow (Gazoduc). In Box: x = [-1 2], y = [9 12]', 'FontSize', 15)
    legend([h1, h2, legend_entries'],{'DPIV analysis (cluster)', 'Golay Filtering','Front / Surges', 'Driftwood', 'Boulder', 'Rolling boulder'}, 'FontSize', 15);
end

%% Change the X-Axis to MM:SS format
fps = 10;
time_interval = time_interval * 60; % 5 minutes in seconds
max_frame = max(get(gca, 'XLim')); % Get the maximum frame number from x-axis limits
time_in_seconds = 0:time_interval:max_frame / fps; % Generate time points every 2 minutes
frame_ticks = time_in_seconds * fps;
minutes = floor(time_in_seconds / 60);
seconds = mod(time_in_seconds, 60);
time_labels = arrayfun(@(min, sec) sprintf('%d:%02d', min, sec), minutes, seconds, 'UniformOutput', false);

% Set the X-axis ticks and labels
xticks(frame_ticks);      % Set the ticks at 5-minute intervals
xticklabels(time_labels); % Set the corresponding labels
xlabel('Time [mm:ss]','FontSize',15);