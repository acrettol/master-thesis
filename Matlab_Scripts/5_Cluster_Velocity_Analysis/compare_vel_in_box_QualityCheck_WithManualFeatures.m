% ETH Zürich, 25.10.2024
% Jordan Aaron
% mod. Raffaele Spielmann
% mod. Arnaud Crettol

% ------------------------
% Script to visualize velocities in a user selected box (vel_in_box), from PIV hillshade analysis
% Manual labels with each classes having a different color is also diplayed at the end
% Note: use Golay Smoothing first
% ------------------------

clc
clear all
close all

addpath(genpath('Tools'));

%% Specify parameters

% Indicate the event date and Sensor name
event_date  = input("Please enter the event date coordinates (e.g. '13.07.2023'): ");
sensor_name = input("Please enter the sensor name (e.g. 'Gazoduc'): ");

% Plot options : Y-Axis limits and X-Axis ticks format
yPlotLim     = input('Please enter the Y-Axis limits (e.g., [0 12]): ');
tickInterval = minutes(5);  % Intervalls of time between ticks (minutes) 

%% Load Data

% load in desired velocity time series (smoothed)
[velFilename, velPath] = uigetfile('*.mat', 'Select a smoothed VELOCITY DATASET (hs_Golay_, etc.)');
disp('Loading Velocity Dataset ... (might take some minutes) ...')
vel1 = load(fullfile(velPath, velFilename)); % load velocity dataset (e.g. "Gazo_smoothed_5cm_oneHS.mat")
beep

%% load in any corresponding point cloud (.ply File) (for visualization purposes)
[ptcFilename, ptcPath] = uigetfile('*.ply', 'Select an (early) Point Cloud of this Event (.ply)');
ptCloudPlot = pcread(fullfile(ptcPath, ptcFilename)); % load ptCloud

% load in manual labels
[manFilename, manPath] = uigetfile('*.mat', 'Select a Mean Manual Velocity Dataset (MeanVelocities_perObjClass_TT.mat)');
MeanVel_perClass_TT = importdata(fullfile(manPath, manFilename)); % load velocity dataset and assign variable name (using "importdata", instead of "load")

%% Define Parameters

% Hillshade Parameters
azimuth = 90;
altitude = 45;
ptCloudRasterSpacing = 0.05;

xLim = [-15 15]; 
yLim = [-50 50]; 
numBoxes = 1; %number of boxes to view

%define filter limits --> not actually used, because so large / small
x_vel_min = -15;
x_vel_max = 15;
y_vel_min = -20;
y_vel_max = 20;
z_vel_min = -2;
z_vel_max = 2;

%% Select and Extract Velocities

% Create a Hillshade from the selected point cloud for plotting as base
[H,xRange,yRange,interpolatedPtCloud,F] = createScanHillshade_Azimuth_Elevation(ptCloudPlot,ptCloudRasterSpacing,xLim,yLim,azimuth,altitude); %generate hillshade for base
frame = mat2gray(H);
R = imref2d(size(frame),xLim,yLim);

%name the velocity time series for the legend
velocity_dataset_names = {'HS Velocity'};%,'camera 2 new interp'};

%draw box where velocities should be extraced
% [xBox,yBox] = smooth_get_box_coord_gui_include_R(frame,vel1.vel_depth_TT,numBoxes,0,2000,R);
% legendString = generate_legend_string(numBoxes,velocity_dataset_names,yBox);

% Specify box coordinates, ideally 15m from sensor (=/= from check dam).
% Keep it the same across all events and stations
figure(Position=[1200 100 200 600])
imshow(frame,R)
xBox = input('Please enter xBox coordinates (e.g., [1 2]): ');
yBox = input('Please enter yBox coordinates (e.g., [1 2]): ');

%get velocity in input boxes
disp('Extracting Velocities in Box ... (might take some seconds) ...')

velocity_in_box_hs_azi_alti = smooth_get_vel_in_multiple_box_magnitude_sum(vel1.vel_depth_TT,xBox,yBox,x_vel_min, x_vel_max, y_vel_min, y_vel_max,0);
velocity_in_box_hs_azi_alti_unsmooth = get_vel_in_multiple_box_magnitude_sum(vel1.vel_depth_TT,xBox,yBox,x_vel_min, x_vel_max, y_vel_min, y_vel_max,0);

%% Plot Extracted Velocities
% plot unsmoothed (h1) and Golay-smoothed (h2) velocity
h1 = plot(vel1.vel_depth_TT.Time,velocity_in_box_hs_azi_alti_unsmooth,'.b','MarkerSize',6);
hold on
h2 = plot(vel1.vel_depth_TT.Time,velocity_in_box_hs_azi_alti,'LineWidth',2);

%% Plot manual labels per classes
matrix_GTL  = [MeanVel_perClass_TT.front_MeanVel MeanVel_perClass_TT.driftW_MeanVel MeanVel_perClass_TT.boulder_MeanVel MeanVel_perClass_TT.rollB_MeanVel];
names_GTL   = MeanVel_perClass_TT.Properties.VariableNames'; 
colors_GTL  = [0 0 0;0 1 0;0.9290 0.6940 0.1250;1 0.5 0.8]; % 1=black=front, 2=green=driftwood, 3=pink=boulder, 4=orange=rolling boulder 
symbols_GTL = {'^', 'd', 's', 'o'};         % classes symbols
classes_GTL = zeros(size(matrix_GTL,1),1);  % classes values
types_GTL   = rand(size(matrix_GTL,1),3);   % classes colors

% Extract each not NaN class values in matrix_GTL, along with their classes names
for i=1:size(matrix_GTL,1)
    for j=1:size(matrix_GTL,2)
        if matrix_GTL(i,j)>0
            classes_GTL(i) = matrix_GTL(i,j); % Class values
            types_GTL(i,:) = colors_GTL(j,:); % Class colors (names)
        end 
    end
end
% Plot each class separately with different symbols
for j = 1:size(matrix_GTL, 2)
    scatter(MeanVel_perClass_TT.Time, matrix_GTL(:, j), 100, colors_GTL(j, :), symbols_GTL{j}, 'filled');
    hold on
end

% Create and use invisible points for legend entries, then combine legend
legend_entries = gobjects(4, 1);  % Preallocate array for legend entries
for j = 1:size(matrix_GTL, 2)
    legend_entries(j) = scatter(NaN, NaN, 2000, colors_GTL(j, :), symbols_GTL{j}, 'filled');
    hold on
end

%% Adjust Plot and X-Axis Ticks
ColorOrder = turbo(length(velocity_dataset_names)*numBoxes);
set(gcf, 'Position', get(0,'Screensize'));
title(sprintf("Mean surface velocity of the %s debris flow (%s). In Box: x = [%d %d], y = [%d %d]", event_date, sensor_name, xBox(1),xBox(2),yBox(1),yBox(2)),'FontSize',15)
legend([h1, h2, legend_entries'],{'DPIV analysis (cluster)', 'Golay Filtering','Front / Surges', 'Driftwood', 'Boulder', 'Rolling boulder'}, 'FontSize', 15);
xlabel('Time [mm:ss]','FontSize',15)
ylabel('Mean area velocity magnitude [m/s]','FontSize',15)
ylim(yPlotLim);
grid on


% Calculate tick positions and convert to custom formatted strings (MM:SS or H:MM:SS)
minDuration = min(vel1.vel_depth_TT.Time);
maxDuration = max(vel1.vel_depth_TT.Time);
tickPositions = minDuration:tickInterval:maxDuration;  % Create an array from min to max with specified interval
tickLabels = arrayfun(@(x) formatDuration(x), tickPositions, 'UniformOutput', false);
set(gca, 'XTick', tickPositions);
set(gca, 'XTickLabel', tickLabels);

%% Save Timetable with Velocities

% create Timetable:
TT_vel = timetable(vel1.vel_depth_TT.Time, velocity_in_box_hs_azi_alti_unsmooth, velocity_in_box_hs_azi_alti, 'VariableNames', {'rawVel', 'smoothVel'});
save TT_Vel_Golay_inBox_raw_smooth.mat TT_vel

%% Function to format the duration as MM:SS or H:MM:SS (X-Axis ticks) - Added by Arnaud Crettol
function formattedStr = formatDuration(duration)
    % Convert duration to total seconds
    totalSeconds = seconds(duration);
    
    % Calculate hours, minutes, and seconds
    hours = floor(totalSeconds / 3600);
    minutes = floor(mod(totalSeconds, 3600) / 60);
    seco = mod(totalSeconds, 60);
    
    % Format the string
    if hours > 0
        formattedStr = sprintf('%d:%02d:%02d', hours, minutes, seco);
    else
        formattedStr = sprintf('%d:%02d', minutes, seco);
    end
end