% ETH Zurich
% 25.10.2024

% Authors / Contributors:
    % Raffaele Spielmann
    % Samuel Huber
    % Arnaud Crettol

% -------------------------------------------
% SURFACE VELOCITY MEASUREMENT (manual) from HILLSHADE
% -------------------------------------------

% Script to measure the surface velocity, by manually labelling features
% (boulders, logs, etc.) in the HILLSHADE signal using the grountTruthLabeler
% and to calculate / plot the corresponding, relevant parameters
% _______________________________________________________________________

clc
clear
close all

%% Timestamps

% run script first to get timestamps


%% Loading of "gTruth" variable from gTL


% load existing groundTruthLabeler-Labels (gTL_Labels) "gTruth"

disp('Select existing groundTruthLabeler-Label (gTruth)')
disp("Loading gTL variable ... (might take a while) ...")
uiopen('load') % user file selection, of "gTruth" variable


%% Define Hillshade Resolution

% define resolution of Hillshades used:
HSres = input('Input resultion of Hillshades (in m) (e.g. 0.05,...): '); % e.g. 5cm


%% EXTRACT gTruth Data for further Calculations

% signal number according to numbers of data sources (video, LiDAR, etc.) 

signalNr = 1; % --> might need to CHANGE, if first signal is NOT hillshade

signalNames = [gTruth.DataSource.SignalName]; %get signal names to access signals
roiDataHS = gTruth.ROILabelData.(signalNames(signalNr)); %Labels in LiDAR

%Inform user about selected Dataset (should be "Gazo_HS" and not anything else) and ask for Confirmation:
fprintf("Following gTruth ROILabelData will be analyzed: \n SIGNAL NAME = %s \n", signalNames(signalNr))

input("Press ENTER to continue")

disp("Starting Analysis...")


%% CALCULATE EuclideanDistance and Velocity

numFeatures = width(roiDataHS); %number of labelled boulders or driftwood pieces, i.e. number of columns

for i = 1 : numFeatures % number of iterations = number of labelled Features

    %Select coordinates of ROI of current Feature
    featureTemp = roiDataHS.(i); % select column of current Feature i
    featureCoordTemp = cell2mat(featureTemp); % extract only rows with values, convert to double

    featureCoordTemp = featureCoordTemp .* HSres; % CONVERSION from pixel to m (according to selected resolution)

    % Convert to CENTER of current bounding box
    featureCoordTempCenter(:,1) = featureCoordTemp(:,1) + 0.5*featureCoordTemp(:,3); % x-coord. (upper left angle + 0.5 width)
    featureCoordTempCenter(:,2) = featureCoordTemp(:,2) + 0.5*featureCoordTemp(:,4); % y-coord. (upper left angle + 0.5 height)

    %Select corresponding timestamps
        % (necessary, b/c ROI might not be labelled over entire timespan; e.g. only every 5th frame)
    logind = ~cellfun(@isempty,featureTemp); % "~" for opposite (i.e. NOT empty); "cellfun" to Apply function to each cell in cell array; "isempty" to check emptiness
    timestampsTemp = roiDataHS.Time(logind); % select relevant timestamps (i.e. where we have featureCoord)
    timestamps{i} = timestampsTemp; % save current timestamps (for calculation of deltaTime betw. frames)


    % for-loop to calculate Euclidean Distance & Velocity between boulder center in subsequent frames

    for k = 1 : (sum(logind)-1) %numbers of labels per boulder; -1 b/c  we calculate distances in between
    
        % Calculate Distances between x,y,z Coordinates:
        DistX = featureCoordTempCenter(k,1) - featureCoordTempCenter(k+1,1); % difference in x-coord., betw. 1st and 2nd row
        DistY = featureCoordTempCenter(k,2) - featureCoordTempCenter(k+1,2); % y-coord.
        
        % Calculate (Euclidean) Distance between Boulder Position in subsequent Frames
        EucDist{i}(k,1) = sqrt(DistX.^2 + DistY.^2);

        % Calculate Timespan between subsequent Frames
            % N.B.: does NOT necessarily need to be 0.1 s, we might have a label only every 5th frame!
        deltaTime = timestamps{i}(k+1,1) - timestamps{i}(k,1); %time passed between frame k and k+1 ("duration" obj.)
        deltaTime = seconds(deltaTime); % convert "duration" to seconds

        % Calculate (and save for plotting) "mean timestamp" for each distance value
            % (inst. velocity will be plotted at the time between the two corresponding frames)
        timestamps_plot{i}(k,1) = mean([timestamps{i}(k,1); timestamps{i}(k+1,1)]); % mean value of time at t(k) and t(k+1)

        % Calculate "mean boulder coord." (i.e. between two labels) for plotting later
        meanFeatureCoordCenter{i}(k,:) = mean([featureCoordTempCenter(k, 1:2); featureCoordTempCenter(k+1, 1:2)]);
        
        % Calculate Velocity and save
        featureInstVel{i}(k,1) = EucDist{i}(k,1) / deltaTime; % instantaneous velocity of given boulder
        
    end

    featureMeanVel(i) = mean(featureInstVel{i}(:,1)); % mean velocity of given boulder
    timestampsMean(i) = mean(timestamps_plot{i}(:,1)); % mean of timestamps, to plot mean velocity
    
    %Calculate "cumulative Euclidean Distance", for plotting distance vs. time
    cumEucDist{i} = cumsum(EucDist{i}, 'reverse'); % ('reverse' = from large to zero; so that it resembles graphs of automated flow velocity measurement)

    clear featureTemp
    clear featureCoordTemp
    clear featureCoordTempCenter

end

%% CONCATENATE Results to Timetables TT

% Convert Timestamps (plot) and Instantaneous Velocities of individual features to one Timetable:

% first feature
InstVel_all_TT = timetable(timestamps_plot{1},featureInstVel{1}, 'VariableNames', roiDataHS.Properties.VariableNames(1)); % note: 'VariableNames' expects "cell array" (ergo: indexing in parentheses)

% add all other features:
for i = 2:numFeatures

    % merge timestamps and inst. vel. of given feature (i)
    TTtemp = timetable(timestamps_plot{i},featureInstVel{i}, 'VariableNames', roiDataHS.Properties.VariableNames(i)); 

    % add to general timetable with all features:
    InstVel_all_TT = synchronize(InstVel_all_TT, TTtemp);

    clear TTtemp
end


% create mean velocity timetable, which is formatted the same way as the instantaneous velocity timetable

% Create a copy of the InstVel_all_TT timetable
MeanVel_all_TT = InstVel_all_TT;

% Initialize the new timetable to have the same dimensions as InstVel_all_TT but with NaN values
MeanVel_all_TT{:, :} = NaN;

% Loop through each column of the InstVel_all_TT timetable
for i = 1:width(InstVel_all_TT)
    % Get the current column of data
    currCol = InstVel_all_TT{:, i};
    
    % Calculate the average of the non-NaN values in the current column
    avgValue = mean(currCol(~isnan(currCol)));
    
    % Calculate the average time of the non-NaN values in the current column
    avgTime = mean(InstVel_all_TT.Time(~isnan(currCol)));
    
    % Find the index of the nearest time value in MeanVelocities_perObject to the average time
    [~, idx] = min(abs(MeanVel_all_TT.Time - avgTime));
    
    % Assign the average value to the row corresponding to the average time in MeanVelocities_perObject
    MeanVel_all_TT{idx, i} = avgValue;
end


%% SEPARATION of Rolling Boulders and Driftwood (for further Analysis) and new object classes

% Separation of labels into Classes

ii   = 0; %index for saving rolling boulder
iii  = 0; %index for saving drift wood
vii  = 0; %index for saving big boulder
viii = 0; %index for saving front
ix   = 0; %index for saving roll wave / surge

% for loop over all labesl
for i = 1:numFeatures

    % check if rolling boulder
    if InstVel_all_TT.Properties.VariableNames{i}(1,1) == 'r' % if starts with "r" (i.e. rollB)

        ii = ii + 1; % increase corresponding index by 1

        rollB_Time(ii,1) = timestampsMean(i); % save corresponding timestamp (i.e. mean of all timestamps of corr. feature)
        rollB_MeanVel(ii,1) = featureMeanVel(i); % save corresponding mean velocity (of given feature i)
          
    % check if driftwood
    elseif InstVel_all_TT.Properties.VariableNames{i}(1,1) == 'd' % if starts with "d" (i.e. driftW)
        
        iii = iii + 1;

        driftW_Time(iii,1) = timestampsMean(i);
        driftW_MeanVel(iii,1) = featureMeanVel(i);

    % check if "normal" boulder    
    elseif InstVel_all_TT.Properties.VariableNames{i}(1,1) == 'B'
        
        vii = vii + 1;

        boulder_Time(vii,1) = timestampsMean(i);
        boulder_MeanVel(vii,1) = featureMeanVel(i);  

    % check if front    
    elseif InstVel_all_TT.Properties.VariableNames{i}(1,1) == 'f'
        
        viii = viii + 1;

        front_Time(viii,1) = timestampsMean(i);
        front_MeanVel(viii,1) = featureMeanVel(i);

    % else: inform user about non-inclusion
    else
        fprintf('Following feature is not included: %s \n', InstVel_all_TT.Properties.VariableNames{i}) % display in command window
    end

end

% Combine in Timetables:
rollB_MeanVel_TT = timetable(rollB_Time, rollB_MeanVel);
driftW_MeanVel_TT = timetable(driftW_Time, driftW_MeanVel);
boulder_MeanVel_TT = timetable(boulder_Time, boulder_MeanVel);
front_MeanVel_TT = timetable(front_Time, front_MeanVel);
%surge_MeanVel_TT = timetable(surge_Time, surge_MeanVel);

% create big timetable with all mean velocities per Object
MeanVel_perClass_TT = synchronize(front_MeanVel_TT,driftW_MeanVel_TT,boulder_MeanVel_TT,rollB_MeanVel_TT); %surge was deleted
MeanVel_perClass_TT.Properties.DimensionNames{1} = 'Time';


%% Plot: Quick Quality Check

% Plot mean velocities of all
figure
plot(InstVel_all_TT.Time, InstVel_all_TT.Variables, 'k.')
hold on
plot(MeanVel_all_TT.Time, MeanVel_all_TT.Variables, 'bo', MarkerFaceColor='auto')
xtickformat("mm:ss.SS")
xlabel("Event time"); ylabel("Velocity (m/s)")
legend("Instantaneous Velocities")


% Plot mean velocities per Class
figure
plot(MeanVel_perClass_TT.Time, MeanVel_perClass_TT.rollB_MeanVel,'square', MarkerFaceColor="#0072BD")
hold on
plot(MeanVel_perClass_TT.Time, MeanVel_perClass_TT.driftW_MeanVel, 'diamond', MarkerFaceColor="#D95319")
hold on
plot(MeanVel_perClass_TT.Time, MeanVel_perClass_TT.boulder_MeanVel, 'o', MarkerFaceColor="#77AC30")
hold on
% can add other classes here if wanted
xtickformat("mm:ss.SS")
xlabel("Event time"); ylabel("Velocity (m/s)")
legend("Rolling boulders",'Driftwood','Boulders')


%% Save timetables of instantaneous and mean velocities

% Select Path where to Save Results

    % __________________________
    % Dialog Box to inform user, that "DataPath" (directory) has to be defined:
    mydlg = warndlg('Please select Folder where results (instantaneous & mean velocities) should be saved (.mat).', 'User Information');
    waitfor(mydlg); % script paused until OK is pushed
    % Select Folder (path) in User Interface:
    StorePath = uigetdir(path);
    % __________________________

cd(StorePath)

save('InstantaneousVelocities_perObject_TT','InstVel_all_TT');  % save instantaneous velocity per object (each object in a separate column)
save('MeanVelocities_perObject_TT','MeanVel_all_TT');           % save mean velocity per object (each object in sep. column)
save('MeanVelocities_perObjClass_TT','MeanVel_perClass_TT');    % save mean velocity per object class (e.g. rollB, driftW, etc.)

% save('gTruth','gTruth');

fprintf("Results (Timetables, gTruth) have been saved succesfully! (Path: %s) \n", StorePath)
