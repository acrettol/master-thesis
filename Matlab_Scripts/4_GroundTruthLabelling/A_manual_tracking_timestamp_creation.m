% ETH Zurich
% 25.10.2024

% Authors / Contributors:
    % Raffaele Spielmann
    % Arnaud Crettol

% -------------------------------------------
% Script to create Timestamps for groundTruthLabeler
% -------------------------------------------

% Run this script before opening gTL
% Note: in the gTL it is important to label 4 distinct features always:
% (respect the capital or small letter at the beginning of the labels)
% Front >> Category "front" and labels "f1, f2 ..."
% Boulders >> Category "Boulders" and labels "B1, B2 ..."
% Driftwood >> Category "driwftwood" and labels "d1, d2 ..."
% Rolling boulders >> Category "rolling_boulders" and labels "rool_B1, roll_B2 ..."
% _______________________________________________________________________

clc
clear
close all

%% Loading of Timestamp File

% Load Timestamp Data
disp("Select a TIMESTAMP file (e.g. Bloom_timestamps.txt), in folder with .mat files")
[timeFilename, timePath] = uigetfile('*.txt', 'Select a TIMESTAMP file (e.g. Bloom_timestamps.txt)');
timestamps = readtable(fullfile(timePath, timeFilename));

% Define First and Last Frame (OPTIONAL)
start_frame = 1;
end_frame   = 74486;

% Create Timestamps
timestamps_eventTime = timestamps.Time - timestamps.Time(1); % convert (absolute) timestampts to (relative) event time timestamps (frame1 = 00:00:00)
timestamps_eT_select = timestamps_eventTime(start_frame:end_frame); % select relevant timestamps
dt = diff(timestamps_eT_select); % calculate difference between "real" timestamps

Time = seconds(timestamps_eT_select); % convert to "duration" format


%% open ground Truth labeler

% open groundTruthLabeler, and select "Time" as input   
groundTruthLabeler
