% 25.10.2024
% Jordan Aaron, modified Raffaele Spielmann and then by Arnaud Crettol

% Script to generate .mat file which can then be used to
% generate .jpg (Hillshades) files from .ply files on the Cluster
clc
clear all
close all

PreProcParams = struct();

%% Define Names and Path
loc_name    = 'Gazoduc';        % Gazoduc, CD29, CD27
sensor_name = 'Gazo';           % Gazo, Bloom, Pip, Owen, Marcel, Merged
event_date  = '2022_07_04';

spacing      = 5; % give the HS spacing in cm (2.5/5/10) >> 5cm is the best
resolution   = [num2str(spacing),'cm'];
if mod(spacing, 1) ~= 0 % if the spacing has decimals, changes the '.' to 'p'
    indexToReplace  = 2;
    newCharacter    = 'p';
    resolution      = [resolution(1:indexToReplace-1), newCharacter, resolution(indexToReplace+1:end)];
end

ply_file_folder   = sprintf('/cluster/scratch/acrettol/Data/%s/%s/PLY_files/%s_ply',event_date,loc_name,sensor_name); %use this line for others
% ply_file_folder   = sprintf('/cluster/scratch/acrettol/Data/%s/%s/PLY_files/Merged_ply',event_date,loc_name); %use this line for CD27 and CD29 (merged)
hs_output_folder  = sprintf('/cluster/scratch/acrettol/Data/%s/%s/Hillshades/%s_%s',event_date,loc_name,sensor_name,resolution);

config_file_save_folder = sprintf('Configuration_Files/%s/%s',event_date,sensor_name);

% Create config folder if they do not exist:
if ~exist(config_file_save_folder, 'dir')
    mkdir(config_file_save_folder);
end

% Save filenames and paths:
config_filename = fullfile(config_file_save_folder,sprintf('%s_HSgeneration_config_%s',sensor_name,resolution));

%% Define Frames, limits, azimuth and altitude (CHANGE)
num_cores   = 82; % up to 128
FirstFrame  = 1;
LastFrame   = 18000;  
spacing     = spacing/100;
xlim        = [-10,10];
ylim        = [-35,35];    
zMax        = 10;    
azi         = 90;
alti        = 45;

% define a box which contains the sensor (and cables evt.) to exclude them
% from the point cloud and then from the projection into a Hillshade ('Yes'
% or 'No'). Added by Arnaud Crettol, June 2024
remove_sensor = "Yes"; 
sensor_box  = [-1 1 -2 5 -2 1]; % [xMin xMax yMin yMax zMin zMax]

%% Save in Struct as mat

PreProcParams.num_cores     = num_cores;
PreProcParams.ply_folder    = ply_file_folder;
PreProcParams.output_folder = hs_output_folder;
PreProcParams.FirstFrame    = FirstFrame;
PreProcParams.LastFrame     = LastFrame;
PreProcParams.num_cores     = num_cores;
PreProcParams.spacing       = spacing;
PreProcParams.xlim          = xlim;
PreProcParams.ylim          = ylim;
PreProcParams.zMax          = zMax;
PreProcParams.azi           = azi;
PreProcParams.alti          = alti;
PreProcParams.remove_sensor    = remove_sensor;
PreProcParams.sensor_box       = sensor_box;


save(sprintf('%s.mat',config_filename)); % save .mat