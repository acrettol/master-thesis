function generate_HS_from_PLY(PathToParamsMatFile)    
% function to PreProcess Point clouds: from .mat files (after decompression from .bag files) to hillshades and .ply point clouds
% ----

addpath(genpath('Tools'));

load(PathToParamsMatFile);  % load 'PreProcessing_Parameters_HS-ply_struct.mat'

%% Define Path and Frames

% define Folder Paths (Input and Output), needs to be done before the processing:
ply_folder    = PreProcParams.ply_folder;
output_folder = PreProcParams.output_folder;
zMax          = PreProcParams.zMax;
spacingg      = PreProcParams.spacing;
xlimit        = PreProcParams.xlim;
ylimit        = PreProcParams.ylim;
azi           = PreProcParams.azi;
alti          = PreProcParams.alti;
remove_sensor = PreProcParams.remove_sensor;
sensor_box    = PreProcParams.sensor_box;

frames = [PreProcParams.FirstFrame : PreProcParams.LastFrame]; %selection of frames which should be turned into hillshade

% define a box which contains the sensor (and cables evt.) to exclude them
% from the point cloud and then from the projection into a Hillshade
xMin = sensor_box(1);
xMax = sensor_box(2);
yMin = sensor_box(3);
yMax = sensor_box(4);
zMin = sensor_box(5);
zMax_pt = sensor_box(6);

%% Process Point Clouds / Generate Hillshades

local_job      = parcluster('local');
pool           = parpool(local_job, PreProcParams.num_cores);

folder_ply    = ply_folder;
folder_output = output_folder;

parfor i = 1:length(frames)

        % Load point clouds, and filterut values with high z-value using zMax or the sensor_box (i.e. raindrops, sensor, etc.) by Arnaud Crettol
        ptCloud1    = pcread(fullfile(folder_ply,sprintf('%0.5d.ply',frames(i))));
        if remove_sensor == "Yes" 
            indices = ptCloud1.Location(:,1) < xMin | ptCloud1.Location(:,1) > xMax | ...
                     ptCloud1.Location(:,2) < yMin | ptCloud1.Location(:,2) > yMax | ...
                     ptCloud1.Location(:,3) < zMin | ptCloud1.Location(:,3) > zMax_pt;
        elseif remove_sensor == "No"
            indices     = ptCloud1.Location(:,3) < zMax;
        end
        ptCloud1    = select(ptCloud1,indices);

        % Check if there are points in (filtered) point cloud (otherwise Hillshade interpolation will break)
        if isempty(ptCloud1.Location)  % if no points, then creates dummy image
    
            HS_img_1 = imread('peppers.png');   % load default Matlab image as dummy
            fprintf('No Hillshade could be generated for Frame = %d \n', frames(i))
    
        else % if not empty --> actually generate hillshade
            
            [HS_1,~,~ , ~,~ ] = createScanHillshade_Azimuth_Elevation(ptCloud1, spacingg, xlimit, ylimit, azi, alti); % generate HS
            HS_img_1 = mat2gray(HS_1);
        end
        
        % save Hillshade
        imwrite(HS_img_1,fullfile(folder_output,sprintf('%0.5d.jpg', frames(i))));

end

pool.delete();


end

% %% Extract and Generate Corresponding Timestamps
% 
% % Find Timestamp File:
%     % Define the file pattern you want to search for
%     filePattern = '*_timestamps.txt';
% 
%     % Use the dir function to list files that match the pattern
%     TimestampFile = dir(fullfile(ply_folder, filePattern));
% 
%     % Check if any matching files were found
%     if isempty(TimestampFile)
%         disp('No Timestamp file found.');
%     else
%         TimestampFile = TimestampFile(1).name; % get matching file
%         disp(['Found Timestamp file: ', TimestampFile]);
%     end
% 
% % Load and Extract Timestamps
% timestamps = readtable(fullfile(ply_folder, TimestampFile)); % read in .txt file
% 
% timeValues = timestamps.Time(frames); % extract time column, for selected frames (as defined above)
% 
% timeZero = timeValues(1); % first frame time = 0 event time
% 
% EventTime = timeValues - timeZero; % subtract first time (= 0) from each time to get "event time"
%                                     % N.B.: time 0 does NOT need to correspond with Trigger, but with first decompressed .bag file!
% lidartime = seconds(EventTime); % save as "lidartime" (for historical reasons)
% lidartime.Format = 'mm:ss.SS'; % optimize time format
% 
% 
% %% Save Timestamps and Variables for Documentation later
% 
% % Save Timestamps
% save(fullfile(output_folder, 'lidartime.mat'), 'lidartime')
