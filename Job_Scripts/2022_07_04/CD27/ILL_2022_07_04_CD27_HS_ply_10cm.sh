#!/bin/bash

#SBATCH --ntasks 1                 # number of cores ?
#SBATCH --cpus-per-task=82         # number of cores (e.g. 82,128)
#SBATCH --mem-per-cpu=2G           # request of RAM
#SBATCH --time=00:50:00
#SBATCH --mail-type=END            # to send email once done
#SBATCH --output="CD27_creation_HS_log_10cm"

#load all necessary modules:
module load matlab/R2022b

#execute your script:
matlab -nodisplay -singleCompThread -sd "/cluster/home/acrettol/master-thesis/Matlab_Scripts/2_Generate_Hillshade" -r "generate_HS_from_PLY(['Configuration_Files/CD27/CD27_HSgeneration_config_10cm.mat'])"