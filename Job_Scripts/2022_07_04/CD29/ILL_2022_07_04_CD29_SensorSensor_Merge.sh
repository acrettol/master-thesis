#!/bin/bash

#SBATCH --ntasks 1                 # number of cores ?
#SBATCH --cpus-per-task=82         # number of cores (e.g. 82,128)
#SBATCH --mem-per-cpu=2G           # request of RAM
#SBATCH --time=01:00:00            # hh:mm:ss
#SBATCH --mail-type=END            # to send email once done
#SBATCH --output="CD29_SensorMerge"

#load all necessary modules:
module load matlab/R2022b

#execute your script:
matlab -nodisplay -singleCompThread -sd "/cluster/home/acrettol/master-thesis/Matlab_Scripts/1_Generate_PLY" -r "C_CD29_merge_generate_PLY(['/cluster/home/acrettol/master-thesis/Matlab_Scripts/1_Generate_PLY/Transformations/ILL/2022/2022_07_04/CD29_PipToBloom_SensorSensor_tform.mat'], ['/cluster/scratch/acrettol/Data/2022_07_04/CD29/PLY_files/Bloom_ply'], ['/cluster/scratch/acrettol/Data/2022_07_04/CD29/PLY_files/Pip_ply'], ['/cluster/scratch/acrettol/Data/2022_07_04/CD29/PLY_files/Merged_ply'], [1], [47169], [82])"