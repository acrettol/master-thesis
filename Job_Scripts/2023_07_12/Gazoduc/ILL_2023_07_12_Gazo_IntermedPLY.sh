#!/bin/bash

#SBATCH --ntasks 1                 # number of cores ?
#SBATCH --cpus-per-task=60         # number of cores (e.g. 82,128)
#SBATCH --mem-per-cpu=2G           # request of RAM
#SBATCH --time=01:30:00
#SBATCH --mail-type=END            # to send email once done
#SBATCH --output="Gazo_creation_PLY_log"

#load all necessary modules:
module load matlab/R2023b

#execute your script:
matlab -nodisplay -singleCompThread -sd "/cluster/home/acrettol/master-thesis/Matlab_Scripts/1_Generate_PLY" -r "B_generate_intermediate_PLY(['Configuration_Files/ILL/2023/2023_07_12/Gazo_2023_07_12_config.mat'])"