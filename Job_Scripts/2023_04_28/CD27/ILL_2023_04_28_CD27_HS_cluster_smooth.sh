#!/bin/bash

#SBATCH --ntasks 1               # number of cores ?
#SBATCH --cpus-per-task=60       # number of cores (e.g. 82,128)
#SBATCH --mem-per-cpu=2G         # request of RAM
#SBATCH --time=00:30:00          # hh:mm:ss
#SBATCH --mail-type=END          # to send email once done
#SBATCH --output="CD27_smoothing_HS_cluster_log.out"

#load all necessary modules:
module load matlab/R2023b

#execute your script:
matlab -nodisplay -singleCompThread -sd "/cluster/home/acrettol/master-thesis/Matlab_Scripts/3_Generate_Hillshade_Cluster" -r "D_Smooth_HS_velocity(['Configuration_Files/ILL/2023/2023_04_28/Merged_2023_04_28_config_HS.mat'])"