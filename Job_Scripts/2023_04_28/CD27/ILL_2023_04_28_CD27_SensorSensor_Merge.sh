#!/bin/bash

#SBATCH --ntasks 1                 # number of cores ?
#SBATCH --cpus-per-task=82         # number of cores (e.g. 82,128)
#SBATCH --mem-per-cpu=2G           # request of RAM
#SBATCH --time=03:00:00            # hh:mm:ss
#SBATCH --mail-type=END            # to send email once done
#SBATCH --output="CD27_SensorMerge"

#load all necessary modules:
module load matlab/R2023b

#execute your script:
matlab -nodisplay -singleCompThread -sd "/cluster/home/acrettol/master-thesis/Matlab_Scripts/1_Generate_PLY" -r "C_CD27_merge_generate_PLY(['/cluster/home/acrettol/master-thesis/Matlab_Scripts/1_Generate_PLY/Transformations/ILL/2023/2023_04_28/CD27_MarcelToOwen_SensorSensor_tform.mat'], ['/cluster/scratch/acrettol/Data/2023_04_28/CD27/PLY_files/Owen_ply'], ['/cluster/scratch/acrettol/Data/2023_04_28/CD27/PLY_files/Marcel_ply'], ['/cluster/scratch/acrettol/Data/2023_04_28/CD27/PLY_files/Merged_ply'], [20500], [21500], [82])"