#!/bin/bash

#SBATCH --ntasks 1                 # number of cores ?
#SBATCH --cpus-per-task=60         # number of cores (e.g. 82,128)
#SBATCH --mem-per-cpu=2G           # request of RAM
#SBATCH --time=07:00:00            # hh:mm:ss
#SBATCH --mail-type=END            # to send email once done
#SBATCH --output="Pip_creation_HS_cluster_log_5cm.out"

#load all necessary modules:
module load matlab/R2023b

#execute your script:
matlab -nodisplay -singleCompThread -sd "/cluster/home/acrettol/master-thesis/Matlab_Scripts/3_Generate_Hillshade_Cluster" -r "C_Get_HS_velocity(['Configuration_Files/ILL/2023/2023_11_14/Pip_2023_11_14_config_HS.mat'])"